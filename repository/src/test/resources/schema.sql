create table CERTIFICATES (
	ID bigserial primary key,
	NAME character varying (250),
	DESCRIPTION character varying (1000),
	PRICE numeric(19,2),
	DATE_OF_CREATION date,
	DATE_OF_MODIFICATION date,
	DATE_OF_EXPIRATION date,
	CREATED_ON timestamp,
	CREATED_BY bigint,
	UPDATED_ON timestamp,
	UPDATED_BY bigint
);

create table TAGS(
	ID bigserial primary key,
	NAME character varying (250),
	CREATED_ON timestamp,
	CREATED_BY bigint,
	UPDATED_ON timestamp,
	UPDATED_BY bigint
);

create table CERTIFICATES_TAGS(
	ID bigserial primary key,
	CERTIFICATE_ID bigint,
	TAG_ID bigint,

	foreign key (CERTIFICATE_ID) references CERTIFICATES(ID) on delete cascade,
	foreign key (TAG_ID) references TAGS(ID) on delete cascade
);

create table USERS(
    ID bigserial primary key,
    PASSWORD character varying (250),
    USER_NAME character varying (250) unique,
    LOCKED boolean,
    EXPIRED boolean,
    CREDENTIALS_EXPIRED boolean,
    ENABLED boolean,
    ROLE character varying (100)
);

create table PURCHASES(
    ID bigserial primary key,
    USER_ID bigint,
    CREATED timestamp default now(),
	CREATED_ON timestamp,
	CREATED_BY bigint,
	UPDATED_ON timestamp,
	UPDATED_BY bigint,

    foreign key(USER_ID) references USERS(ID) on delete cascade
);

create table PURCHASES_CERTIFICATES(
    ID bigserial primary key,
    PURCHASE_ID bigint,
    CERTIFICATE_ID bigint,

    foreign key (PURCHASE_ID) references PURCHASES(ID) on delete cascade,
    foreign key (CERTIFICATE_ID) references CERTIFICATES(ID) on delete cascade
);

create or replace view most_money_spent_user_tags_popularity
as
select users.id AS most_money_spent_user_id,
    count(tags.id) AS used_times,
    sum(certificates.price) AS money_spent,
	tags.* from tags
join certificates_tags on tags.id = certificates_tags.tag_id
join certificates on certificates_tags.certificate_id = certificates.id
join purchases_certificates on certificates.id = purchases_certificates.certificate_id
join purchases on purchases_certificates.purchase_id = purchases.id
join users on purchases.user_id = users.id
where users.id in
    (
    select users.id from users
    join purchases on users.id = purchases.user_id
    join purchases_certificates on purchases.id = purchases_certificates.purchase_id
    join certificates on purchases_certificates.certificate_id = certificates.id
    group by users.id, certificates.id
    order by sum(certificates.price) desc
    limit (1)
    )
group by tags.id, users.id
order by used_times desc, money_spent desc;

create or replace view most_popular_tags_among_all_users
 as
select count(tags.id) as used_times, sum(certificates.price) as cost_of_call_certificates, tags.* from tags
left join certificates_tags on tags.id = certificates_tags.tag_id
left join certificates on certificates_tags.certificate_id = certificates.id
left join purchases_certificates on certificates.id = purchases_certificates.certificate_id
left join purchases on purchases_certificates.purchase_id = purchases.id
left join users on purchases.user_id = users.id
group by tags.id
order by used_times desc, cost_of_call_certificates desc;