insert into CERTIFICATES (NAME, DESCRIPTION, PRICE, DATE_OF_CREATION, DATE_OF_MODIFICATION, DATE_OF_EXPIRATION) values (    'Tested certificate name',    'Tested certificate description',    20.99,    current_date,    null,    '2020-07-03');
insert into CERTIFICATES (NAME, DESCRIPTION, PRICE, DATE_OF_CREATION, DATE_OF_MODIFICATION, DATE_OF_EXPIRATION) values (    'Certificate to be changed',    'Certificate description to be changed',    20.99,    current_date,    null,    '2020-07-03');
insert into CERTIFICATES (NAME, DESCRIPTION, PRICE, DATE_OF_CREATION, DATE_OF_MODIFICATION, DATE_OF_EXPIRATION) values (    'Specific name',    'Certificate description',    20.99,    current_date,    null,    '2020-07-03');
insert into CERTIFICATES (NAME, DESCRIPTION, PRICE, DATE_OF_CREATION, DATE_OF_MODIFICATION, DATE_OF_EXPIRATION) values (    'Specific name',    'Certificate description',    20.99,    current_date,    null,    '2020-07-03');

insert into TAGS (NAME) values ('Tested tag name');
insert into TAGS (NAME) values ('Tag name to be changed');
insert into TAGS (NAME) values ('Tag to be ignored');
insert into TAGS (NAME) values ('Tag to be ignored');
insert into TAGS (NAME) values ('Name for query test');
insert into TAGS (NAME) values ('Name for query test');

insert into CERTIFICATES_TAGS(CERTIFICATE_ID, TAG_ID) values (1, 1);

insert into USERS (USER_NAME, PASSWORD, LOCKED, EXPIRED, CREDENTIALS_EXPIRED, ENABLED, ROLE) values (    'user',    '$2a$10$q4XkF24mWgTtCR.A5/JJCuXXKErrcm3aCbX9B.ihWG.ozcPjHtqpe',    false,    false,    false,    true,    'User');
insert into USERS (USER_NAME, PASSWORD, LOCKED, EXPIRED, CREDENTIALS_EXPIRED, ENABLED, ROLE) values (    'guest',    '$2a$10$q4XkF24mWgTtCR.A5/JJCuXXKErrcm3aCbX9B.ihWG.ozcPjHtqpe',    false,    false,    false,    true,    'Guest');
insert into USERS (USER_NAME, PASSWORD, LOCKED, EXPIRED, CREDENTIALS_EXPIRED, ENABLED, ROLE) values (    'admin',    '$2a$10$q4XkF24mWgTtCR.A5/JJCuXXKErrcm3aCbX9B.ihWG.ozcPjHtqpe',    false,    false,    false,    true,    'Administrator');