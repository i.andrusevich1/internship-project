package com.epam.esm.core.repository;

import com.epam.esm.core.config.RepositorySpringConfigurationForTest;
import com.epam.esm.core.entity.Certificate;
import com.epam.esm.core.entity.Tag;
import com.epam.esm.core.entity.User;
import com.epam.esm.core.exception.RepositoryException;
import com.epam.esm.core.specification.impl.CertificateSpecification;
import com.epam.esm.core.specification.impl.parameter.CertificateParameters;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RepositorySpringConfigurationForTest.class)
@Transactional
@WithMockUser
public class CertificateRepositoryImplTest {
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private EntityRepository<Certificate> testedRepository;

    @Before
    public void initialize(){
        entityManager.clear();
        User user = new User();
        user.setId(1);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(
                        user, null, null);
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    }

    @Test
    public void testGetById() throws RepositoryException {
        //given
        Certificate expected = new Certificate();
        expected.setId(3);
        expected.setName("Specific name");
        expected.setDescription("Certificate description");
        expected.setPrice(BigDecimal.valueOf(20.99));
        expected.setDateOfCreation(LocalDate.now());
        expected.setDateOfModification(null);
        expected.setDateOfExpiration(LocalDate.of(2020, 7, 3));
        expected.setTags(Collections.EMPTY_SET);
        expected.setPurchases(Collections.EMPTY_SET);

        //when
        Certificate actual = testedRepository.getById(3);

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = RepositoryException.class)
    public void testGetByIdThrowsException() throws RepositoryException {
        //given

        //when
        testedRepository.getById(100500);

        //then
        Assert.fail();
    }

    @Test
    public void testAdd() throws RepositoryException {
        //given
        Certificate expected = new Certificate();
        expected.setName("Certificate name");
        expected.setDescription("Certificate description");
        expected.setPrice(BigDecimal.valueOf(30.5));
        expected.setDateOfCreation(LocalDate.now());
        expected.setDateOfExpiration(LocalDate.of(2020, 9, 15));
        Tag tag = new Tag();
        tag.setId(2);
        Set<Tag> tags = new HashSet<>();
        tags.add(tag);
        expected.setTags(tags);

        //when
        Certificate actual = testedRepository.add(expected);
        expected.setId(5);

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testAddWithSomeNullValues() throws RepositoryException {
        //given
        Certificate expected = new Certificate();
        expected.setName("Certificate name");
        expected.setDescription("Certificate description");
        expected.setPrice(BigDecimal.TEN);
        expected.setDateOfCreation(LocalDate.now());
        expected.setDateOfExpiration(LocalDate.of(2020,01,01));
        expected.setTags(Collections.EMPTY_SET);

        //when
        Certificate actual = testedRepository.add(expected);
        expected.setId(6);
        expected.setPrice(BigDecimal.ZERO);
        expected.setDateOfExpiration(LocalDate.of(169104628, 12, 9));

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = RepositoryException.class)
    public void testAddThrowsException() throws RepositoryException {
        //given
        Certificate expected = new Certificate();
        expected.setName("Certificate name");
        Mockito.when(testedRepository.getById(Mockito.anyLong())).then(x -> x.getMethod().invoke(testedRepository, 0));

        //when
        testedRepository.add(expected);

        //then
        Assert.fail();
    }

    @Test
    public void testRemoveWithExistingEntity() throws RepositoryException {
        //given
        Certificate certificate = new Certificate();
        certificate.setId(1);
        certificate.setName("Tested certificate name");
        certificate.setDescription("Tested certificate description");
        certificate.setPrice(BigDecimal.valueOf(20.99));
        certificate.setDateOfCreation(LocalDate.now());
        certificate.setDateOfModification(null);
        certificate.setDateOfExpiration(LocalDate.of(2020,07,03));

        //when
        testedRepository.remove(certificate);

        //then
        //test is successful if no exceptions are thrown
    }

    @Test(expected = RepositoryException.class)
    public void testRemoveWithNonExistingEntity() throws RepositoryException {
        //given
        long id = 100500;
        Certificate certificate = new Certificate();
        certificate.setId(id);

        //when
        testedRepository.remove(certificate);

        //then
        Assert.fail();
    }

    @Test
    public void testUpdateWithExistingEntity() throws RepositoryException {
        //given
        Certificate expected = new Certificate();
        expected.setId(2);
        expected.setName("Changed certificate name");
        expected.setDescription("Changed certificate description");
        expected.setPrice(BigDecimal.valueOf(50.12));
        expected.setDateOfCreation(LocalDate.now());
        expected.setDateOfModification(LocalDate.now());
        expected.setDateOfExpiration(LocalDate.of(2022, 3, 10));
        expected.setTags(Collections.EMPTY_SET);
        expected.setPurchases(Collections.EMPTY_SET);

        //when
        Certificate actual = testedRepository.update(expected);

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = RepositoryException.class)
    public void testUpdateWithNonExistingEntity() throws RepositoryException {
        //given
        Certificate expected = new Certificate();
        expected.setId(100501);
        expected.setName("Changed certificate name");
        expected.setDescription("Changed certificate description");
        expected.setPrice(BigDecimal.valueOf(50.12));
        expected.setDateOfCreation(LocalDate.now());
        expected.setDateOfModification(LocalDate.now());
        expected.setDateOfExpiration(LocalDate.of(2022, 3, 10));
        expected.setTags(Collections.EMPTY_SET);

        //when
        testedRepository.update(expected);
        entityManager.flush();

        //then
        Assert.fail();
    }

    @Test
    public void testQuery() throws RepositoryException {
        //given
        String searchedName = "Specific name";
        CertificateSpecification specificationForTest
                = new CertificateSpecification(CertificateParameters.NAME, "Specific name");
        List<Certificate> expected = new ArrayList<>();
        Certificate first = new Certificate();
        first.setId(3);
        first.setName(searchedName);
        first.setDescription("Certificate description");
        first.setPrice(BigDecimal.valueOf(20.99));
        first.setDateOfCreation(LocalDate.now());
        first.setDateOfModification(null);
        first.setDateOfExpiration(LocalDate.of(2020, 7, 3));
        first.setTags(Collections.EMPTY_SET);
        first.setPurchases(Collections.EMPTY_SET);
        Certificate second = new Certificate();
        second.setId(4);
        second.setName(searchedName);
        second.setDescription("Certificate description");
        second.setPrice(BigDecimal.valueOf(20.99));
        second.setDateOfCreation(LocalDate.now());
        second.setDateOfModification(null);
        second.setDateOfExpiration(LocalDate.of(2020, 7, 3));
        second.setTags(Collections.EMPTY_SET);
        second.setPurchases(Collections.EMPTY_SET);
        expected.add(first);
        expected.add(second);

        //when
        List<Certificate> actual = testedRepository.query(specificationForTest);

        //then
        Assert.assertEquals(expected, actual);
    }
}
