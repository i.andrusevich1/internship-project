package com.epam.esm.core.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@PropertySource("classpath:db_config.properties")
@Import(RepositorySpringConfiguration.class)
public class RepositorySpringConfigurationForTest {
    private static final String DRIVER = "driver";
    private static final String URL = "url";
    private static final String USER = "user";
    private static final String PASSWORD = "password";
    private static final String SCHEMA = "schema";

    @Bean
    @Primary
    public DataSource dataSource(Environment environment) {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(environment.getProperty(URL));
        config.setUsername(environment.getProperty(USER));
        config.setPassword(environment.getProperty(PASSWORD));
        config.setDriverClassName(environment.getProperty(DRIVER));
        config.setSchema(environment.getProperty(SCHEMA));
        return new HikariDataSource(config);
    }

    @Bean
    @Primary
    public Properties hibernateProperties() {
        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty(
                "hibernate.hbm2ddl.auto", "create");
        hibernateProperties.setProperty(
                "hibernate.dialect", "org.hibernate.dialect.PostgreSQL82Dialect");
        hibernateProperties.setProperty(
                "show_sql", "true");
        hibernateProperties.put("hibernate.allow_update_outside_transaction", "true");
        return hibernateProperties;
    }
}