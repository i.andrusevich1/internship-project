package com.epam.esm.core.config;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RepositorySpringConfigurationForTest.class)
public class SpringContextTest {
    @Autowired
    private ApplicationContext context;
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private DataSource dataSource;

    @Test
    public void testContextCreation() {
        //given

        //when

        //then
        Assert.assertNotNull(context);
    }

    @Test
    public void testEntityManagerCreation() {
        //given

        //when

        //then
        Assert.assertNotNull(entityManager);
    }

    @Test
    public void testDataSourceCreation() {
        //given

        //when

        //then
        Assert.assertNotNull(dataSource);
    }
}
