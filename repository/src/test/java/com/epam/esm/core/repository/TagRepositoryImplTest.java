package com.epam.esm.core.repository;

import com.epam.esm.core.config.RepositorySpringConfigurationForTest;
import com.epam.esm.core.entity.Tag;
import com.epam.esm.core.entity.User;
import com.epam.esm.core.exception.RepositoryException;
import com.epam.esm.core.specification.impl.TagSpecification;
import com.epam.esm.core.specification.impl.parameter.TagParameters;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RepositorySpringConfigurationForTest.class)
@Transactional
@WithMockUser
public class TagRepositoryImplTest {
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private TagRepository testedRepository;

    @Before
    public void initialize(){
        entityManager.clear();
        User user = new User();
        user.setId(1);
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(
                        user, null, null);
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
    }

    @Test
    public void testGetById() throws RepositoryException {
        //given
        Tag expected = new Tag();
        expected.setId(1);
        expected.setName("Tested tag name");

        //when
        Tag actual = testedRepository.getById(1);

        //then
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testGetByName() throws RepositoryException {
        //given
        Tag expected = new Tag();
        expected.setId(1);
        expected.setName("Tested tag name");

        //when
        Tag actual = testedRepository.addOrGetByName(expected);

        //then
        Assert.assertEquals(actual, expected);
    }

    @Test(expected = RepositoryException.class)
    public void testGetByIdThrowsException() throws RepositoryException {
        //given

        //when
        testedRepository.getById(100500);
        //then
    }

    @Test
    public void testAdd() throws RepositoryException {
        //given
        Tag expected = new Tag();
        String name = "Tag name for addTag() method test";
        expected.setName(name);

        //when
        Tag actual = testedRepository.add(expected);
        expected.setId(8);

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testRemoveWithExistingEntity() throws RepositoryException {
        //given
        Tag tag = new Tag();
        tag.setId(1);
        tag.setName("Tested tag name");

        //when
        testedRepository.remove(tag);

        //then
        // test is successful if no exceptions are thrown
    }

    @Test(expected = RepositoryException.class)
    public void testRemoveWithNonExistingEntity() throws RepositoryException {
        //given
        long id = 100500;
        String name = "Removed tag name";
        Tag tag = new Tag();
        tag.setId(id);
        tag.setName(name);

        //when
        testedRepository.remove(tag);
        entityManager.flush();

        //then
        Assert.fail();
    }

    @Test
    public void testQuery() throws RepositoryException {
        //given
        String searchedName = "Name for query test";
        List<Tag> expected = new ArrayList<>();
        Tag first = new Tag();
        first.setName(searchedName);
        first.setId(5);
        expected.add(first);
        TagSpecification specificationForTest = new TagSpecification(TagParameters.NAME, searchedName);

        //when
        List<Tag> actual = testedRepository.query(specificationForTest);

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testUpdateWithExistingEntity() throws RepositoryException {
        //given
        long id = 2;
        String name = "Changed tag name";
        Tag expected = new Tag();
        expected.setId(id);
        expected.setName(name);

        //when
        Tag actual = testedRepository.update(expected);

        //then
        Assert.assertEquals(expected, actual);
    }

    public void testUpdateWithNonExistingEntity() throws RepositoryException {
        //given
        long id = 100500;
        String name = "Changed tag name";
        Tag expected = new Tag();
        expected.setId(id);
        expected.setName(name);

        //when
        testedRepository.update(expected);
        entityManager.flush();

        //then
        Assert.fail();
    }
}
