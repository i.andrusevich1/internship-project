package com.epam.esm.core.repository.impl;

import com.epam.esm.core.entity.User;
import com.epam.esm.core.repository.AbstractRepository;
import com.epam.esm.core.repository.EntityRepository;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryImpl extends AbstractRepository<User> {

    public UserRepositoryImpl() {
        super(User.class);
    }
}
