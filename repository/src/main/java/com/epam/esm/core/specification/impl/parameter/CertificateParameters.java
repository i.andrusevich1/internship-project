package com.epam.esm.core.specification.impl.parameter;

import java.util.List;

public enum CertificateParameters {
    ID("id", null),
    NAME("name", null),
    DESCRIPTION("description", null),
    PRICE("price", null),
    DATEOFCREATION(null, null),
    DATEOFMODIFICATION(null, null),
    DATEOFEXPIRATION(null, null),
    TAGID("id", List.of("tags")),
    TAGNAME("name", List.of("tags")),
    USERID("id", List.of("purchases", "user")),
    NAMEPART(null, null),
    DESCRIPTIONPART(null, null);

    private String fieldName;
    private List<String> joinTablesNames;

    CertificateParameters(String fieldName, List<String> joinTablesNames) {
        this.fieldName = fieldName;
        this.joinTablesNames = joinTablesNames;
    }

    public String getFieldName() {
        return fieldName;
    }

    public List<String> getJoinTables() {
        return joinTablesNames;
    }
}

