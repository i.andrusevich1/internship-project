package com.epam.esm.core.specification.impl.parameter;

import java.util.List;

public enum  UserParameters {
    ID("id", null),
    USERNAME("userName", null),
    PASSWORD("password", null),
    LOCKED("locked", null),
    EXPIRED("expired", null),
    CREDENTIALSEXPIRED("credentialsExpired", null),
    ENABLED("enabled", null),
    ROLE("role", null);

    private String fieldName;
    private List<String> joinTablesNames;

    UserParameters(String fieldName, List<String> joinTablesNames) {
        this.fieldName = fieldName;
        this.joinTablesNames = joinTablesNames;
    }

    public String getFieldName() {
        return fieldName;
    }

    public List<String> getJoinTableName(){
        return joinTablesNames;
    }
}
