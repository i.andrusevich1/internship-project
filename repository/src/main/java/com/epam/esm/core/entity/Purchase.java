package com.epam.esm.core.entity;

import com.epam.esm.core.entity.audit.Audit;
import com.epam.esm.core.entity.audit.AuditListener;
import com.epam.esm.core.entity.audit.Auditable;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "PURCHASES")
@EntityListeners({AuditListener.class})
public class Purchase implements Auditable, Identifiable, Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "USER_ID", nullable = false)
    private User user;
    @ManyToMany(cascade = CascadeType.REFRESH)
    @JoinTable(name = "PURCHASES_CERTIFICATES",
            joinColumns = @JoinColumn(name = "PURCHASE_ID"),
            inverseJoinColumns = @JoinColumn(name = "CERTIFICATE_ID"))
    private List<Certificate> certificates;
    @CreationTimestamp
    @Column(updatable = false)
    private LocalDateTime created;
    @Embedded
    private Audit audit;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUse() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Certificate> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<Certificate> certificates) {
        this.certificates = certificates;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    @Override
    public Audit getAudit() {
        return audit;
    }

    @Override
    public void setAudit(Audit audit) {
        this.audit = audit;
    }

}
