package com.epam.esm.core.entity;

public interface Identifiable {
    long getId();
}
