package com.epam.esm.core.exception;

public class InvalidSortParameterException extends Exception {
    public InvalidSortParameterException() {
    }

    public InvalidSortParameterException(String message) {
        super(message);
    }

    public InvalidSortParameterException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidSortParameterException(Throwable cause) {
        super(cause);
    }
}
