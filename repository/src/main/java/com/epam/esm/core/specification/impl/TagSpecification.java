package com.epam.esm.core.specification.impl;

import com.epam.esm.core.entity.Certificate;
import com.epam.esm.core.entity.Purchase;
import com.epam.esm.core.entity.Tag;
import com.epam.esm.core.exception.RepositoryException;
import com.epam.esm.core.specification.AbstractSpecification;
import com.epam.esm.core.specification.impl.parameter.TagParameters;

import javax.persistence.criteria.*;
import java.util.*;

public class TagSpecification extends AbstractSpecification<Tag> {

    public TagSpecification() {
    }

    public TagSpecification(TagParameters parameter, String value) throws RepositoryException {
        acceptFilterParameter(parameter.name(), Collections.singletonList(value));
    }

    @Override
    protected boolean isParameterSupported(String parameterName) {
        return defineParameter(parameterName) != null;
    }

    @Override
    protected String defineFieldName(String parameterName) {
        return defineParameter(parameterName).getFieldName();
    }

    @Override
    protected boolean isJoinNeeded(String parameterName) {
        return defineParameter(parameterName).getJoinTables() != null;
    }

    @Override
    protected boolean isParameterCustom(String parameter) {
        return defineParameter(parameter).getFieldName() == null;
    }

    @Override
    protected List<String> getJoinedTables(String parameterName) {
        return defineParameter(parameterName).getJoinTables();
    }

    @Override
    public void fulfillQueryWithCustomFilters(CriteriaQuery<Tag> criteriaQuery, CriteriaBuilder criteriaBuilder,
                                              Root<Tag> root) {
        Set<Map.Entry<String, List<String>>> filtersAndTheirMultipleValues = getCustomFilterParameters().entrySet();
        for (Map.Entry<String, List<String>> filterWithItsValues : filtersAndTheirMultipleValues) {
            String filter = filterWithItsValues.getKey();
            for (String value : filterWithItsValues.getValue()) {
                switch (defineParameter(filter)) {
                    case POPULARITY: {
                        Join<Tag, Certificate> certificates = root.join("certificates");
                        Join<Certificate, Purchase> purchases = certificates.join("purchases");
                        criteriaQuery
                                .groupBy(root.get("id"))
                                .having(criteriaBuilder.equal(criteriaBuilder.count(root.get("id")), value));
                        break;
                    }
                    case TOTALSUM: {
                        Join<Tag, Certificate> certificates = root.join("certificates");
                        Join<Certificate, Purchase> purchases = certificates.join("purchases");
                        criteriaQuery
                                .groupBy(root.get("id"))
                                .having(criteriaBuilder.equal(criteriaBuilder.sum(certificates.get("price")), value));
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void fulfillQueryWithCustomSorts(CriteriaQuery<Tag> criteriaQuery, CriteriaBuilder criteriaBuilder,
                                            Root<Tag> root) {
        Set<Map.Entry<String, Boolean>> sortsAndTheirOrders = getCustomSortParameters().entrySet();
        for (Map.Entry<String, Boolean> sortAndItsOrder : sortsAndTheirOrders) {
            String sort = sortAndItsOrder.getKey();
            Boolean ascendingOrder = sortAndItsOrder.getValue();
            switch (defineParameter(sort)) {
                case POPULARITY: {
                    Join<Tag, Certificate> certificates = root.join("certificates");
                    Join<Certificate, Purchase> purchases = certificates.join("purchases");
                    criteriaQuery.groupBy(root.get("id"));
                    Expression<Long> count = criteriaBuilder.count(root.get("id"));
                    if (ascendingOrder) {
                        criteriaQuery.orderBy(criteriaBuilder.asc(count));
                    } else {
                        criteriaQuery.orderBy(criteriaBuilder.desc(count));
                    }
                    break;
                }
                case TOTALSUM: {
                    Join<Tag, Certificate> certificates = root.join("certificates");
                    Join<Certificate, Purchase> purchases = certificates.join("purchases");
                    criteriaQuery.groupBy(root.get("id"));
                    Expression<Number> price = criteriaBuilder.sum(certificates.get("price"));
                    if (ascendingOrder) {
                        criteriaQuery.orderBy(criteriaBuilder.asc(price));
                    } else {
                        criteriaQuery.orderBy(criteriaBuilder.desc(price));
                    }
                }
            }
        }
    }

    private TagParameters defineParameter(String parameterName) {
        for (TagParameters parameters : TagParameters.values()) {
            if (parameters.name().equals(parameterName)) {
                return parameters;
            }
        }
        return null;
    }
}
