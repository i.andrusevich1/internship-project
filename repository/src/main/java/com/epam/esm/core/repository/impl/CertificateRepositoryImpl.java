package com.epam.esm.core.repository.impl;

import com.epam.esm.core.entity.Certificate;
import com.epam.esm.core.exception.RepositoryException;
import com.epam.esm.core.repository.AbstractRepository;
import org.springframework.stereotype.Repository;

@Repository
public class CertificateRepositoryImpl extends AbstractRepository<Certificate>{

    public CertificateRepositoryImpl() {
        super(Certificate.class);
    }

    @Override
    public Certificate update(Certificate certificate) throws RepositoryException {
        try {
            Certificate persistent = super.update(certificate);
            entityManager().flush();             // needed to make next line possible
            entityManager().refresh(persistent); // needed to make entity contain update timestamp
            return persistent;
        } catch (Exception e) {
            throw new RepositoryException(e);
        }
    }
}
