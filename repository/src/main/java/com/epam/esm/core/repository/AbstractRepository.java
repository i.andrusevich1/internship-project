package com.epam.esm.core.repository;

import com.epam.esm.core.entity.Identifiable;
import com.epam.esm.core.exception.RepositoryException;
import com.epam.esm.core.specification.Specification;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.List;

public abstract class AbstractRepository<T extends Identifiable> implements EntityRepository<T> {
    @PersistenceContext
    private EntityManager entityManager;
    private Class<T> entityClass;

    public AbstractRepository(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public T getById(long id) throws RepositoryException {
        T item = entityManager.find(entityClass, id);
        if (item == null) {
            throw new RepositoryException("Entity of class " + entityClass + " with ID " + id + " was not found");
        }
        return item;
    }

    @Override
    public T add(T item) throws RepositoryException {
        try {
            entityManager.persist(item);
            entityManager.flush();       // needed to get entity id
            entityManager.refresh(item); // needed to get valid related entities in case only their id's where provided
                                         // also needed to get creation timestamps
            return item;
        } catch (Exception e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    public void remove(T item) throws RepositoryException {
        try {
            entityManager.remove(entityManager.contains(item)
                    ? item
                    : getById(item.getId()));
        } catch (Exception e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    public T update(T item) throws RepositoryException {
        try {
            getById(item.getId());
            return entityManager.merge(item);
        } catch (Exception e) {
            throw new RepositoryException(e);
        }
    }

    @Override
    public List<T> query(Specification<T> specification) throws RepositoryException {
        try {
            CriteriaQuery<T> query = prepareQuery(specification);
            int currentPage = specification.getCurrentPage();
            int itemsPerPage = specification.getItemsPerPage();
            return entityManager
                    .createQuery(query)
                    .setFirstResult((currentPage - 1) * itemsPerPage)
                    .setMaxResults(itemsPerPage)
                    .getResultList();
        } catch (Exception e) {
            throw new RepositoryException(e);
        }
    }

    protected EntityManager entityManager() {
        return entityManager;
    }

    private CriteriaQuery<T> prepareQuery(Specification<T> specification){
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityClass);
        Root<T> root = criteriaQuery.from(entityClass);
        Predicate[] predicates = specification.createPredicates(criteriaBuilder, root);
        Order[] orders = specification.createOrders(criteriaBuilder, root);
        criteriaQuery.select(root).where(predicates).orderBy(orders);
        specification.fulfillQueryWithCustomFilters(criteriaQuery, criteriaBuilder, root);
        specification.fulfillQueryWithCustomSorts(criteriaQuery, criteriaBuilder, root);
        return criteriaQuery;
    }
}
