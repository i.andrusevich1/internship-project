package com.epam.esm.core.specification.impl.parameter;

import java.util.List;

public enum  PurchaseParameters {
    ID("id", null),
    CREATED(null, null),
    USERID("id", List.of("user")),
    CERTIFICATEID("id", List.of("certificates")),
    TAGNAME("name", List.of("certificates", "tags"));


    private String fieldName;
    private List<String> joinTablesNames;

    PurchaseParameters(String fieldName, List<String> joinTablesNames) {
        this.fieldName = fieldName;
        this.joinTablesNames = joinTablesNames;
    }


    public String getFieldName() {
        return fieldName;
    }

    public List<String> getJoinTables() {
        return joinTablesNames;
    }
}
