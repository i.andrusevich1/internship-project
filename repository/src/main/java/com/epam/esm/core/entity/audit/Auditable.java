package com.epam.esm.core.entity.audit;

public interface Auditable {
    Audit getAudit();

    void setAudit(Audit audit);
}
