package com.epam.esm.core.repository.impl;

import com.epam.esm.core.entity.Purchase;
import com.epam.esm.core.repository.AbstractRepository;
import com.epam.esm.core.repository.EntityRepository;
import org.springframework.stereotype.Repository;

@Repository
public class PurchaseRepositoryImpl extends AbstractRepository<Purchase> {

    public PurchaseRepositoryImpl() {
        super(Purchase.class);
    }
}
