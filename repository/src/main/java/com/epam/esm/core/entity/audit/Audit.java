package com.epam.esm.core.entity.audit;

import com.epam.esm.core.entity.User;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Embeddable
public class Audit implements Serializable {
    @ManyToOne
    @JoinColumn(name = "CREATED_BY", updatable = false)
    private User createdBy;
    @Column(name = "CREATED_ON", updatable = false)
    private LocalDateTime createdOn;
    @ManyToOne
    @JoinColumn(name = "UPDATED_BY", insertable = false)
    private User updatedBy;
    @Column(name = "UPDATED_ON", insertable = false)
    private LocalDateTime updatedOn;

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(LocalDateTime createdOn) {
        this.createdOn = createdOn;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public LocalDateTime getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(LocalDateTime updatedOn) {
        this.updatedOn = updatedOn;
    }
}
