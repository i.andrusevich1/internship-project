package com.epam.esm.core.repository.impl;

import com.epam.esm.core.entity.Tag;
import com.epam.esm.core.exception.RepositoryException;
import com.epam.esm.core.repository.AbstractRepository;
import com.epam.esm.core.repository.TagRepository;
import com.epam.esm.core.specification.impl.TagSpecification;
import com.epam.esm.core.specification.impl.parameter.TagParameters;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TagRepositoryImpl extends AbstractRepository<Tag> implements TagRepository{

    public TagRepositoryImpl() {
        super(Tag.class);
    }

    @Override
    public Tag addOrGetByName(Tag tag) throws RepositoryException {
        List<Tag> tags = query(new TagSpecification(TagParameters.NAME, tag.getName()));
        if (tags.isEmpty()) {
            return add(tag);
        }
        return tags.get(0);
    }

    @Override
    public List<Tag> getMostMonetSpentUserTagsByPopularity() {
        return entityManager().createNativeQuery("select * from most_money_spent_user_tags_popularity", Tag.class).getResultList();
    }
}
