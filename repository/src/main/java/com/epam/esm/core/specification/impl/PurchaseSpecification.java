package com.epam.esm.core.specification.impl;

import com.epam.esm.core.entity.Purchase;
import com.epam.esm.core.exception.RepositoryException;
import com.epam.esm.core.specification.AbstractSpecification;
import com.epam.esm.core.specification.impl.parameter.PurchaseParameters;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PurchaseSpecification extends AbstractSpecification<Purchase> {

    public PurchaseSpecification() {
    }

    public PurchaseSpecification(PurchaseParameters parameter, String value) throws RepositoryException {
        acceptFilterParameter(parameter.name(), Collections.singletonList(value));
    }

    @Override
    protected boolean isParameterSupported(String parameterName) {
        return defineParameter(parameterName) != null;
    }

    @Override
    protected String defineFieldName(String parameterName) {
        return defineParameter(parameterName).getFieldName();
    }

    @Override
    protected boolean isJoinNeeded(String parameterName) {
        return defineParameter(parameterName).getJoinTables() != null;
    }

    @Override
    protected boolean isParameterCustom(String parameterName) {
        return defineParameter(parameterName).getFieldName() == null;
    }

    @Override
    protected List<String> getJoinedTables(String parameterName) {
        return defineParameter(parameterName).getJoinTables();
    }

    @Override
    public void fulfillQueryWithCustomFilters(CriteriaQuery<Purchase> criteriaQuery, CriteriaBuilder criteriaBuilder,
                                              Root<Purchase> root) {
        Set<Map.Entry<String, List<String>>> filtersAndTheirMultipleValues = getCustomFilterParameters().entrySet();
        for (Map.Entry<String, List<String>> filterWithItsValues : filtersAndTheirMultipleValues) {
            String filter = filterWithItsValues.getKey();
            for (String value : filterWithItsValues.getValue()) {
                if (defineParameter(filter) == PurchaseParameters.CREATED) {
                    criteriaQuery
                            .where(criteriaBuilder.equal(root.get("created"),
                                    LocalDateTime.parse(value, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS"))));
                }
            }
        }
    }

    @Override
    public void fulfillQueryWithCustomSorts(CriteriaQuery<Purchase> criteriaQuery, CriteriaBuilder criteriaBuilder,
                                            Root<Purchase> root) {
        Set<Map.Entry<String, Boolean>> sortsAndTheirOrders = getCustomSortParameters().entrySet();
        for (Map.Entry<String, Boolean> sortAndItsOrder : sortsAndTheirOrders) {
            String sort = sortAndItsOrder.getKey();
            Boolean ascendingOrder = sortAndItsOrder.getValue();
            if (defineParameter(sort) == PurchaseParameters.CREATED) {
                if (ascendingOrder) {
                    criteriaQuery.orderBy(criteriaBuilder.asc(root.get("created")));
                } else {
                    criteriaQuery.orderBy(criteriaBuilder.desc(root.get("created")));
                }
            }
        }
    }

    private PurchaseParameters defineParameter(String parameterName) {
        for (PurchaseParameters parameters : PurchaseParameters.values()) {
            if (parameters.name().equals(parameterName)) {
                return parameters;
            }
        }
        return null;
    }
}
