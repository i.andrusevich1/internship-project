package com.epam.esm.core.repository;

import com.epam.esm.core.entity.Tag;
import com.epam.esm.core.exception.RepositoryException;

import java.util.List;

public interface TagRepository extends EntityRepository<Tag>{

    /**
     * Whenever Certificate is created or updated all the yet nonexistent Tags
     * embedded in the Certificate must be added to the storage.
     *
     * @param tag - Tag to be added to the storage or to be retrieved from the storage if already exists
     * @return - Tag retrieved from the storage (either just-created or existing before) with Id in it
     * @throws RepositoryException when something goes wrong while adding or retrieving tag
     */
    Tag addOrGetByName(Tag tag) throws RepositoryException;

    List<Tag> getMostMonetSpentUserTagsByPopularity() throws RepositoryException;
}
