package com.epam.esm.core.entity;

import com.epam.esm.core.entity.audit.Audit;
import com.epam.esm.core.entity.audit.AuditListener;
import com.epam.esm.core.entity.audit.Auditable;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "CERTIFICATES")
@EntityListeners({AuditListener.class})
public class Certificate implements Auditable, Identifiable, Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String description;
    @Column(nullable = false)
    private BigDecimal price;
    @Column(name = "DATE_OF_CREATION", updatable = false)
    @CreationTimestamp
    private LocalDate dateOfCreation;
    @Column(name = "DATE_OF_MODIFICATION", insertable = false)
    @UpdateTimestamp
    private LocalDate dateOfModification;
    @Column(name = "DATE_OF_EXPIRATION", nullable = false)
    private LocalDate dateOfExpiration;
    @ManyToMany
    @JoinTable(name = "CERTIFICATES_TAGS",
            joinColumns = @JoinColumn(name = "CERTIFICATE_ID"),
            inverseJoinColumns = @JoinColumn(name = "TAG_ID"))
    private Set<Tag> tags;
    @ManyToMany(mappedBy = "certificates")
    private Set<Purchase> purchases;
    @Embedded
    private Audit audit;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public LocalDate getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(LocalDate dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public LocalDate getDateOfModification() {
        return dateOfModification;
    }

    public void setDateOfModification(LocalDate dateOfModification) {
        this.dateOfModification = dateOfModification;
    }

    public LocalDate getDateOfExpiration() {
        return dateOfExpiration;
    }

    public void setDateOfExpiration(LocalDate dateOfExpiration) {
        this.dateOfExpiration = dateOfExpiration;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Set<Purchase> getPurchases() {
        return purchases;
    }

    public void setPurchases(Set<Purchase> purchases) {
        this.purchases = purchases;
    }

    @Override
    public Audit getAudit() {
        return audit;
    }

    @Override
    public void setAudit(Audit audit) {
        this.audit = audit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Certificate that = (Certificate) o;
        return id == that.id &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                (this.price == null) ? (that.price == null) : (price.compareTo(that.price) == 0) &&
                Objects.equals(dateOfCreation, that.dateOfCreation) &&
                Objects.equals(dateOfModification, that.dateOfModification) &&
                Objects.equals(dateOfExpiration, that.dateOfExpiration) &&
                Objects.equals(tags, that.tags) &&
                Objects.equals(purchases, that.purchases);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, price, dateOfCreation, dateOfModification, dateOfExpiration, tags, purchases);
    }

    @Override
    public String toString() {
        return "Certificate{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", dateOfCreation=" + dateOfCreation +
                ", dateOfModification=" + dateOfModification +
                ", dateOfExpiration=" + dateOfExpiration +
                ", tags=" + tags +
                ", purchases=" + purchases +
                '}';
    }
}
