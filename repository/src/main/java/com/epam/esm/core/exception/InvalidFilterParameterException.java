package com.epam.esm.core.exception;

public class InvalidFilterParameterException extends Exception {
    public InvalidFilterParameterException() {
    }

    public InvalidFilterParameterException(String message) {
        super(message);
    }

    public InvalidFilterParameterException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidFilterParameterException(Throwable cause) {
        super(cause);
    }
}
