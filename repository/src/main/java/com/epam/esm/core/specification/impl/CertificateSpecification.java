package com.epam.esm.core.specification.impl;

import com.epam.esm.core.entity.Certificate;
import com.epam.esm.core.exception.RepositoryException;
import com.epam.esm.core.specification.AbstractSpecification;
import com.epam.esm.core.specification.impl.parameter.CertificateParameters;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CertificateSpecification extends AbstractSpecification<Certificate> {

    public CertificateSpecification() {
    }

    public CertificateSpecification(CertificateParameters parameter, String value) throws RepositoryException {
        acceptFilterParameter(parameter.name(), Collections.singletonList(value));
    }

    @Override
    protected boolean isParameterSupported(String parameterName) {
        return defineParameter(parameterName) != null;
    }

    @Override
    protected String defineFieldName(String parameterName) {
        return defineParameter(parameterName).getFieldName();
    }

    @Override
    protected boolean isJoinNeeded(String parameterName) {
        return defineParameter(parameterName).getJoinTables() != null;
    }

    @Override
    protected boolean isParameterCustom(String parameterName) {
        return defineParameter(parameterName).getFieldName() == null;
    }

    @Override
    protected List<String> getJoinedTables(String parameterName) {
        return defineParameter(parameterName).getJoinTables();
    }

    @Override
    public void fulfillQueryWithCustomFilters(CriteriaQuery<Certificate> criteriaQuery, CriteriaBuilder criteriaBuilder,
                                              Root<Certificate> root) {
        Set<Map.Entry<String, List<String>>> filtersAndTheirMultipleValues = getCustomFilterParameters().entrySet();
        for (Map.Entry<String, List<String>> filterWithItsValues : filtersAndTheirMultipleValues) {
            String filter = filterWithItsValues.getKey();
            for (String value : filterWithItsValues.getValue()) {
                switch (defineParameter(filter)) {
                    case NAMEPART: {
                        criteriaQuery.where(criteriaBuilder.like(root.get("name"), "%" + value + "%"));
                        break;
                    }
                    case DESCRIPTIONPART:{
                        criteriaQuery.where(criteriaBuilder.like(root.get("description"), "%" + value + "%"));
                        break;
                    }
                    case DATEOFCREATION:{
                        criteriaQuery.where(criteriaBuilder.equal(root.get("dateOfCreation"), LocalDate.parse(value)));
                        break;
                    }
                    case DATEOFMODIFICATION:{
                        criteriaQuery.where(criteriaBuilder.equal(root.get("dateOfModification"), LocalDate.parse(value)));
                        break;
                    }
                    case DATEOFEXPIRATION:{
                        criteriaQuery.where(criteriaBuilder.equal(root.get("dateOfExpiration"), LocalDate.parse(value)));
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void fulfillQueryWithCustomSorts(CriteriaQuery<Certificate> criteriaQuery, CriteriaBuilder criteriaBuilder,
                                            Root<Certificate> root) {
        Set<Map.Entry<String, Boolean>> sortsAndTheirOrders = getCustomSortParameters().entrySet();
        for (Map.Entry<String, Boolean> sortAndItsOrder : sortsAndTheirOrders) {
            String sort = sortAndItsOrder.getKey();
            Boolean ascendingOrder = sortAndItsOrder.getValue();
            switch (defineParameter(sort)) {
                case NAMEPART: {
                    if (ascendingOrder) {
                        criteriaQuery.orderBy(criteriaBuilder.asc(root.get("name")));
                    } else {
                        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("name")));
                    }
                    break;
                }
                case DESCRIPTIONPART:{
                    if (ascendingOrder) {
                        criteriaQuery.orderBy(criteriaBuilder.asc(root.get("description")));
                    } else {
                        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("description")));
                    }
                    break;
                }
                case DATEOFCREATION:{
                    if (ascendingOrder) {
                        criteriaQuery.orderBy(criteriaBuilder.asc(root.get("dateOfCreation")));
                    } else {
                        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("dateOfCreation")));
                    }
                    break;
                }
                case DATEOFMODIFICATION:{
                    if (ascendingOrder) {
                        criteriaQuery.orderBy(criteriaBuilder.asc(root.get("dateOfModification")));
                    } else {
                        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("dateOfModification")));
                    }
                    break;
                }
                case DATEOFEXPIRATION:{
                    if (ascendingOrder) {
                        criteriaQuery.orderBy(criteriaBuilder.asc(root.get("dateOfExpiration")));
                    } else {
                        criteriaQuery.orderBy(criteriaBuilder.desc(root.get("dateOfExpiration")));
                    }
                    break;
                }
            }
        }
    }

    private CertificateParameters defineParameter(String parameterName) {
        for (CertificateParameters parameters : CertificateParameters.values()){
            if (parameters.name().equals(parameterName)){
                return parameters;
            }
        }
        return null;
    }
}
