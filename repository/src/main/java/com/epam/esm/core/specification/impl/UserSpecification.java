package com.epam.esm.core.specification.impl;

import com.epam.esm.core.entity.User;
import com.epam.esm.core.exception.RepositoryException;
import com.epam.esm.core.specification.AbstractSpecification;
import com.epam.esm.core.specification.impl.parameter.UserParameters;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collections;
import java.util.List;

public class UserSpecification extends AbstractSpecification<User> {

    public UserSpecification(){}

    public UserSpecification(UserParameters parameter, String value) throws RepositoryException {
        acceptFilterParameter(parameter.name(), Collections.singletonList(value));
    }

    @Override
    protected boolean isParameterSupported(String parameterName) {
        return defineParameter(parameterName) != null;
    }

    @Override
    protected String defineFieldName(String parameterName) {
        return defineParameter(parameterName).getFieldName();
    }

    @Override
    protected boolean isJoinNeeded(String parameterName) {
        return defineParameter(parameterName).getJoinTableName() != null;
    }

    @Override
    protected boolean isParameterCustom(String parameter) {
        return false;
    }

    @Override
    protected List<String> getJoinedTables(String parameterName) {
        return defineParameter(parameterName).getJoinTableName();
    }

    @Override
    public void fulfillQueryWithCustomFilters(CriteriaQuery<User> criteriaQuery, CriteriaBuilder criteriaBuilder,
                                              Root<User> root) {
        // No custom filters provided for User entity
    }

    @Override
    public void fulfillQueryWithCustomSorts(CriteriaQuery<User> criteriaQuery, CriteriaBuilder criteriaBuilder,
                                            Root<User> root) {
        // No custom sorts provided for User entity
    }


    private UserParameters defineParameter(String parameterName) {
        for (UserParameters parameters : UserParameters.values()){
            if (parameters.name().equals(parameterName)){
                return parameters;
            }
        }
        return null;
    }
}
