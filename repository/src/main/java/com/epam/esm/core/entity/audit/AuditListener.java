package com.epam.esm.core.entity.audit;

import com.epam.esm.core.entity.User;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.time.LocalDateTime;

@Component
public class AuditListener {
    @PrePersist
    public void prePersist(Auditable auditable) {
        Audit audit = auditable.getAudit();
        if (audit == null) {
            audit = new Audit();
            auditable.setAudit(audit);
        }
        audit.setCreatedOn(LocalDateTime.now());
        audit.setCreatedBy(getCurrentUser());
    }

    @PreUpdate
    public void preUpdate(Auditable auditable) {
        Audit audit = auditable.getAudit();
        if (audit == null) {
            audit = new Audit();
            auditable.setAudit(audit);
        }
        audit.setUpdatedOn(LocalDateTime.now());
        audit.setUpdatedBy(getCurrentUser());
    }

    private User getCurrentUser() {
        return ((User) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal());
    }
}
