package com.epam.esm.core.specification;

import com.epam.esm.core.exception.InvalidFilterParameterException;
import com.epam.esm.core.exception.InvalidSortParameterException;
import com.epam.esm.core.exception.RepositoryException;

import javax.persistence.criteria.*;
import java.util.*;

public abstract class AbstractSpecification<T> implements Specification<T> {
    private Map<String, List<String>> filterParameters = new LinkedHashMap<>();
    private Map<String, List<String>> customFilterParameters = new LinkedHashMap<>();
    private Map<String, Boolean> sortParameters = new LinkedHashMap<>();
    private Map<String, Boolean> customSortParameters = new LinkedHashMap<>();
    private int itemsPerPage = 10;
    private int currentPage = 1;

    public void acceptFilterParameter(String parameter, List<String> values) throws RepositoryException {
        parameter = parameter.toUpperCase();
        if (!isParameterSupported(parameter)) {
            throw new RepositoryException(
                    new InvalidFilterParameterException("Filter parameter " + parameter + " is not supported"));
        }
        if (isParameterCustom(parameter)) {
            customFilterParameters.put(parameter, values);
        } else {
            filterParameters.put(parameter, values);
        }
    }

    public void acceptSortParameter(String parameter, Boolean ascending) throws RepositoryException {
        parameter = parameter.toUpperCase();
        if (!isParameterSupported(parameter)) {
            throw new RepositoryException(
                    new InvalidSortParameterException("Sort parameter " + parameter + " is not supported"));
        }
        if (isParameterCustom(parameter)) {
            customSortParameters.put(parameter, ascending);
        } else {
            sortParameters.put(parameter, ascending);
        }
    }

    @Override
    public Predicate[] createPredicates(CriteriaBuilder criteriaBuilder, Root root) {
        List<Predicate> predicates = new ArrayList<>();
        Set<Map.Entry<String, List<String>>> filtersAndTheirMultipleValues = filterParameters.entrySet();
        for (Map.Entry<String, List<String>> filterWithItsValues : filtersAndTheirMultipleValues) {
            String filter = filterWithItsValues.getKey();
            for (String value : filterWithItsValues.getValue()) {
                if (isJoinNeeded(filter)) {
                    Join lastJoin = null;
                    for (String joinedTable : getJoinedTables(filter)) {
                        lastJoin = (lastJoin == null) ? root.join(joinedTable) : lastJoin.join(joinedTable);
                    }
                    predicates.add(criteriaBuilder.equal(lastJoin.get(defineFieldName(filter)), value));
                } else {
                    predicates.add(criteriaBuilder.equal(root.get(defineFieldName(filter)), value));
                }
            }
        }
        return predicates.toArray(new Predicate[0]);
    }

    @Override
    public Order[] createOrders(CriteriaBuilder criteriaBuilder, Root root) {
        List<Order> orders = new ArrayList<>();
        Set<Map.Entry<String, Boolean>> sortsAndTheirOrders = sortParameters.entrySet();
        for (Map.Entry<String, Boolean> sortAndItsOrder : sortsAndTheirOrders) {
            String sort = sortAndItsOrder.getKey();
            Boolean ascendingOrder = sortAndItsOrder.getValue();
            Path path = root.get(defineFieldName(sort));
            orders.add(ascendingOrder ? criteriaBuilder.asc(path) : criteriaBuilder.desc(path));
        }
        return orders.toArray(new Order[0]);
    }

    @Override
    public int getItemsPerPage() {
        return itemsPerPage;
    }

    @Override
    public void setItemsPerPage(int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }

    @Override
    public int getCurrentPage() {
        return currentPage;
    }

    @Override
    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    protected Map<String, List<String>> getFilterParameters() {
        return filterParameters;
    }

    protected Map<String, List<String>> getCustomFilterParameters() {
        return customFilterParameters;
    }

    protected Map<String, Boolean> getSortParameters() {
        return sortParameters;
    }

    protected Map<String, Boolean> getCustomSortParameters() {
        return customSortParameters;
    }

    protected abstract boolean isParameterSupported(String parameterName);

    protected abstract String defineFieldName(String parameter);

    protected abstract boolean isParameterCustom(String parameter);

    protected abstract boolean isJoinNeeded(String parameter);

    protected abstract List<String> getJoinedTables(String parameter);
}
