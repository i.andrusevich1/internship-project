package com.epam.esm.core.repository;

import com.epam.esm.core.exception.RepositoryException;
import com.epam.esm.core.specification.Specification;

import java.util.List;

public interface EntityRepository<T> {
    /**
     * Retrieves entity from the storage by ID
     *
     * @param id - id of the retrieved entity
     * @throws RepositoryException in case retrieval failed
     */
    T getById(long id) throws RepositoryException;

    /**
     * Saves entity into the storage.
     *
     * @param item- entity that should be added to the storage. In case entity has id it is ignored
     * @return - new entity retrieved from the storage with its id
     * @throws RepositoryException in case retrieval of the saved entity failed
     */
    T add(T item) throws RepositoryException;

    /**
     * Deletes entity from the storage
     *
     * @param item- entity that should be deleted from the storage.
     *            Deletion is based on Id stored inside the parameter-entity
     * @throws RepositoryException in case removal of the entity failed
     */
    void remove(T item) throws RepositoryException;

    /**
     * Replaces all the entity parameters.
     * All the entity fields get rewritten (accept Id) even in case they are null
     *
     * @param item - entity that should be updated in the storage
     * @return - renewed entity retrieved from the storage
     * @throws RepositoryException in case retrieval of the updated entity failed
     */
    T update(T item) throws RepositoryException;

    /**
     * Retrieves from the storage all the entities that satisfy the provided specification
     *
     * @param specification - object that stores all the filter and sort parameters
     *                      and provides method to apply them when retrieving entities from the storage
     * @return - list of entities from the storage that satisfy the specification
     * @see Specification
     */
    List<T> query(Specification<T> specification) throws RepositoryException;
}
