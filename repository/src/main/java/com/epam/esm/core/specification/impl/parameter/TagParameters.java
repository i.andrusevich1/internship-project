package com.epam.esm.core.specification.impl.parameter;

import java.util.List;

public enum TagParameters {
    ID("id", null),
    NAME("name", null),
    CERTIFICATEID("id", List.of("certificates")),
    POPULARITY(null, null),
    TOTALSUM(null, null);

    private String fieldName;
    private List<String> joinTablesNames;

    TagParameters(String fieldName, List<String> joinTablesNames) {
        this.fieldName = fieldName;
        this.joinTablesNames = joinTablesNames;
    }

    public String getFieldName() {
        return fieldName;
    }

    public List<String> getJoinTables() {
        return joinTablesNames;
    }
}

