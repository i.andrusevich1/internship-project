package com.epam.esm.core.specification;

import com.epam.esm.core.exception.RepositoryException;

import javax.persistence.criteria.*;
import java.util.List;

public interface Specification<T> {

    /**
     * Takes filter parameter and its value for further processing (e.g. "name"="example")
     *
     * @param parameter - filter parameter name (ignored if invalid)
     * @param values    - filter parameter values
     */
    void acceptFilterParameter(String parameter, List<String> values) throws RepositoryException;

    /**
     * Takes sorting parameter and sorting order for further processing (e.g. "dateOfCreation"="true")
     *
     * @param parameter - sorting parameter name
     * @param ascending - sorting order where true=ASC
     */
    void acceptSortParameter(String parameter, Boolean ascending) throws RepositoryException;

    int getItemsPerPage();


    int getCurrentPage();

    Predicate[] createPredicates(CriteriaBuilder criteriaBuilder, Root root);

    Order[] createOrders(CriteriaBuilder criteriaBuilder, Root root);

    void fulfillQueryWithCustomFilters(CriteriaQuery<T> criteriaQuery, CriteriaBuilder criteriaBuilder, Root<T> root);

    void fulfillQueryWithCustomSorts(CriteriaQuery<T> criteriaQuery, CriteriaBuilder criteriaBuilder, Root<T> root);

    void setItemsPerPage(int itemsPerPage);

    void setCurrentPage(int currentPage);
}