create function descriptionIncluding (text)
returns setof certificates
as $$
    select distinct * from CERTIFICATES where DESCRIPTION like concat('%', $1, '%');
$$ language sql;

create function nameIncluding (text)
returns setof certificates
as $$
    select distinct * from CERTIFICATES where NAME like concat('%', $1, '%');
$$ language sql;