package com.epam.esm.web.config;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = WebAppInitializer.class)
public class SpringContextTest {
    @Autowired
    private ApplicationContext context;
    @Autowired
    private WebApplicationContext webContext;

    @Test
    public void testIfApplicationContextCreated(){
        //given

        //when

        //then
        Assert.assertNotNull(context);
    }

    @Test
    public void testIfWebCApplicationContextCreated(){
        //given

        //when

        //then
        Assert.assertNotNull(webContext);
    }
}
