package com.epam.esm.web.controller;

import com.epam.esm.service.exception.ServiceException;
import com.epam.esm.service.dto.CertificateDto;
import com.epam.esm.service.service.CertificateService;
import com.epam.esm.web.exception.ControllerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/certificates")
public class CertificateController {
    private CertificateService certificateService;

    @Autowired
    public CertificateController(CertificateService certificateService) {
        this.certificateService = certificateService;
    }

    @GetMapping(value = "/{id}")
    public CertificateDto get(@PathVariable long id) throws ControllerException {
        try {
            return certificateService.getById(id);
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping
    public List<CertificateDto> getAll(@RequestParam MultiValueMap<String, String> parameters) throws ControllerException {
        try {
            return certificateService.query(parameters);
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAuthority('Administrator')")
    public CertificateDto add(@RequestBody CertificateDto certificateDto) throws ControllerException {
        try {
            return certificateService.add(certificateDto);
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('Administrator')")
    public CertificateDto put(@RequestBody CertificateDto certificateDto, @PathVariable long id) throws ControllerException {
        certificateDto.setId(id);
        try {
            return certificateService.update(certificateDto);
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAuthority('Administrator')")
    public void delete(@PathVariable long id) throws ControllerException {
        CertificateDto certificateDto = new CertificateDto();
        certificateDto.setId(id);
        try {
            certificateService.remove(certificateDto);
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('Administrator')")
    public CertificateDto patch(@PathVariable long id, @RequestBody CertificateDto certificateDto) throws ControllerException {
        certificateDto.setId(id);
        try {
            return certificateService.patch(certificateDto);
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.NOT_FOUND);
        }
    }
}
