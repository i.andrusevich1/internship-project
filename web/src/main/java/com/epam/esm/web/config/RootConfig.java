package com.epam.esm.web.config;

import com.epam.esm.service.config.ServiceSpringConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@ComponentScan(basePackages={"com.epam.esm.web"},
        excludeFilters={
                @Filter(type= FilterType.ANNOTATION, value= EnableWebMvc.class)
        })
@Import(ServiceSpringConfiguration.class)
public class RootConfig {
}
