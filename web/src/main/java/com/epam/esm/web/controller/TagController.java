package com.epam.esm.web.controller;

import com.epam.esm.service.exception.ServiceException;
import com.epam.esm.service.dto.TagDto;
import com.epam.esm.service.service.TagService;
import com.epam.esm.web.exception.ControllerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/tags")
public class TagController {
    private TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping(value = "/{id}")
    public TagDto get(@PathVariable long id) throws ControllerException {
        try {
            return tagService.getById(id);
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping
    public List<TagDto> getAll(@RequestParam MultiValueMap<String, String> parameters) throws ControllerException {
        try {
            return tagService.query(parameters);
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/popular")
    @PreAuthorize("hasAuthority('Administrator')")
    public List<TagDto> getMostMoneySpentUserTagsByPopularity() throws ControllerException {
        try {
            return tagService.getMostMoneySpentUserTagsByPopularity();
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    @PreAuthorize("hasAuthority('Administrator')")
    @ResponseStatus(HttpStatus.CREATED)
    public TagDto add(@RequestBody TagDto tagDto) throws ControllerException {
        try {
            return tagService.add(tagDto);
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('Administrator')")
    public TagDto put(@RequestBody TagDto tagDto, @PathVariable long id) throws ControllerException {
        tagDto.setId(id);
        try {
            return tagService.update(tagDto);
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('Administrator')")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id) throws ControllerException {
        TagDto tagDto = new TagDto();
        tagDto.setId(id);
        try {
            tagService.remove(tagDto);
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('Administrator')")
    public TagDto patch(@PathVariable long id, @RequestBody TagDto tagDto) throws ControllerException {
        tagDto.setId(id);
        try {
            return tagService.patch(tagDto);
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.NOT_FOUND);
        }
    }
}
