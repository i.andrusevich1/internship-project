package com.epam.esm.web.exception;

import org.springframework.http.HttpStatus;

public class ControllerException extends Exception {
    private HttpStatus httpStatus;

    public ControllerException(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public ControllerException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public ControllerException(String message, Throwable cause, HttpStatus httpStatus) {
        super(message, cause);
        this.httpStatus = httpStatus;
    }

    public ControllerException(Throwable cause, HttpStatus httpStatus) {
        super(cause);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }
}
