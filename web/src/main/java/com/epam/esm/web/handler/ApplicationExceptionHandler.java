package com.epam.esm.web.handler;

import com.epam.esm.web.exception.ControllerException;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger logger = Logger.getLogger(ApplicationExceptionHandler.class);

    @ExceptionHandler(value = ControllerException.class)
    public ResponseEntity<Exception> handle(ControllerException exception){
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(exception.getHttpStatus());
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    public ResponseEntity<Exception> handle(AccessDeniedException exception){
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Exception> handle(Exception exception){
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
