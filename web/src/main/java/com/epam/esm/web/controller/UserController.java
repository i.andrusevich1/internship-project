package com.epam.esm.web.controller;

import com.epam.esm.core.entity.User;
import com.epam.esm.service.dto.CertificateDto;
import com.epam.esm.service.dto.PurchaseDto;
import com.epam.esm.service.dto.UserDto;
import com.epam.esm.service.exception.ServiceException;
import com.epam.esm.service.service.CertificateService;
import com.epam.esm.service.service.PurchaseService;
import com.epam.esm.service.service.UserService;
import com.epam.esm.web.exception.ControllerException;
import com.epam.esm.web.security.jwt.JwtResponse;
import com.epam.esm.web.security.jwt.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@CrossOrigin
public class UserController {
    private static final String USER_ID_FILTER_PARAMETER_NAME = "userId";
    private UserService userService;
    private CertificateService certificateService;
    private PurchaseService purchaseService;
    private JwtTokenUtil jwtTokenUtil;
    private AuthenticationManager authenticationManager;

    @Autowired
    public UserController(UserService userService, CertificateService certificateService,
                          PurchaseService purchaseService, JwtTokenUtil jwtTokenUtil,
                          AuthenticationManager authenticationManager) {
        this.userService = userService;
        this.certificateService = certificateService;
        this.purchaseService = purchaseService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody UserDto dto) throws ControllerException {
        authenticate(dto.getUserName(), dto.getPassword());
        UserDetails userDetails = userService.loadUserByUsername(dto.getUserName());
        String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    @PostMapping("/signup")
    public ResponseEntity<?> signUp(@RequestBody UserDto dto) throws ControllerException {
        try {
            userService.add(dto);
            return createAuthenticationToken(dto);
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("{id}/certificates")
    @PreAuthorize("hasAuthority('Administrator')")
    public List<CertificateDto> getCertificates(@PathVariable String id,
                                                @RequestParam MultiValueMap<String, String> parameters)
            throws ControllerException {
        parameters.set(USER_ID_FILTER_PARAMETER_NAME, id);
        try {
            return certificateService.query(parameters);
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/purchases")
    @PreAuthorize("hasAuthority('User') or hasAuthority('Administrator')")
    public List<PurchaseDto> get(@RequestParam MultiValueMap<String, String> parameters) throws ControllerException {
        try {
            User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            parameters.put(USER_ID_FILTER_PARAMETER_NAME, List.of(Long.toString(currentUser.getId())));
            return purchaseService.query(parameters);
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("{id}/purchases")
    @PreAuthorize("hasAuthority('Administrator')")
    public List<PurchaseDto> getPurchases(@PathVariable String id,
                                          @RequestParam MultiValueMap<String, String> parameters)
            throws ControllerException {
        parameters.set(USER_ID_FILTER_PARAMETER_NAME, id);
        try {
            return purchaseService.query(parameters);
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.NOT_FOUND);
        }
    }

    private void authenticate(String username, String password) throws ControllerException {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new ControllerException(e, HttpStatus.FORBIDDEN);
        } catch (BadCredentialsException e) {
            throw new ControllerException(e, HttpStatus.BAD_REQUEST);
        }
    }
}
