package com.epam.esm.web.controller;

import com.epam.esm.service.dto.PurchaseDto;
import com.epam.esm.service.exception.ServiceException;
import com.epam.esm.service.service.PurchaseService;
import com.epam.esm.web.exception.ControllerException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/purchases")
public class PurchaseController {
    private PurchaseService purchaseService;

    public PurchaseController(PurchaseService purchaseService){
        this.purchaseService = purchaseService;
    }

    @PostMapping
    @PreAuthorize("hasAuthority('User')")
    public PurchaseDto add(@RequestBody PurchaseDto purchaseDto) throws ControllerException {
        try {
            return purchaseService.add(purchaseDto);
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    @PreAuthorize("hasAuthority('Administrator')")
    public List<PurchaseDto> get(@RequestParam MultiValueMap<String, String> parameters) throws ControllerException {
        try {
            return purchaseService.query(parameters);
        } catch (ServiceException e) {
            throw new ControllerException(e, HttpStatus.BAD_REQUEST);
        }
    }
}
