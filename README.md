# Gift Certificates

This project includes back-end source code of a web-application that provides REST services to perform CRUD operations with Gift-certificates.

The project consists of 3 modules:
*  repository - responsible for storing and retrieving data from the database
*  service - responsible for transforming objects from what they look like after retrieving from the database into what they will be delivered to the client and vice versa
*  web - responsible for providing end-points to REST services. Accepts and returnes JSON
 
## Credentials for remote work with the project 

Apart from getting the project onto you local machine you can use remote machine to rebuild the project or get access to the server running the application

| System | Link | Admins credentials (login/password) | User credentials (login/password) |
| ------ | ------ | ------ | ------ |
| Jenkins | http://EPBYMINW8117:8081/job/EPAM%20GLO-ODC%20Mentoring/ | admin/12345678 | developer/12345678
| Tomcat | http://EPBYMINW8117:8080/gift-certificates-app-1.0-SNAPSHOT	 |   || 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

To install and run the application you may need:
* [JDK 11+](https://jdk.java.net/11/) - JDK
* [Maven](https://maven.apache.org/) - Builder
* [PostgreSQL 9.+ ](https://www.postgresql.org/download/) - Database
* [Apache Tomcat 9.+ ](https://tomcat.apache.org/download-90.cgi) - Servlet Container
* [Postman](https://www.getpostman.com/downloads/) - API executor

### Installing


1.  Download or clone the project
2.  Create a new schema in your database
3.  Run the following sql-scripts in your database:
    *  src/test/resources/schema.sql
    *  src/main/resources/functions.sql
4.  Specify your database parameters in the following file:
    *  src/main/resources/db_config.properties
```
Most likely you will need to change only URL to your database and password

url=jdbc:postgresql://localhost:5432/*YOUR_DATABASE_NAME_HERE*
user = postgres
password = *YOUR_PASSWORD_HERE*
driver = org.postgresql.Driver
```
5. Build the project with Maven 
```
Go to the project directory in command line and use command 
>  mvn clean install
```
6. Deploy the .war file that was created after the build onto Tomcat
```
You can find it the following directory:
epam-glo-odc-mentoring\web\target
```
7. Call the application API methods using Postman with the help of the [following API documentation](https://documenter.getpostman.com/view/10010270/SWT5igTj)


## Running the tests

Maven runs all the unit and integration tests automatically when building the project but you can run the tests independently:
```
Go to the project directory in command line and use command 
>  mvn test
```

## Code analysis
You can get the [SonarQube](https://www.sonarqube.org/downloads) analysis of the project. 
1.  Install the [SonarQube](https://www.sonarqube.org/downloads)
2.  Go to the project directory in command line and use command 
```
>  mvn sonar:sonar
```
3. Open [SonarQube](https://www.sonarqube.org/downloads) in you browser and find *gift-certificates-parent* project


## Authors

**Ivan Andrusevich** 
* [UPSA](http://upsa.epam.com/workload/employeeView.do?employeeId=4060741400389512361)
* [Telescope](https://telescope.epam.com/who/key/upsaId/4060741400389512361)


