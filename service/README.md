# Service module

This module is responsible for transforming objects from what they look like after retrieving from the database into what they will be delivered to the client and vice versa. 
Basically, convertes DTOs into entities and entities into DTOs. 