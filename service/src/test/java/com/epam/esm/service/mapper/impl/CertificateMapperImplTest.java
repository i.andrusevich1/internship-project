package com.epam.esm.service.mapper.impl;

import com.epam.esm.core.entity.Tag;
import com.epam.esm.service.dto.CertificateDto;
import com.epam.esm.core.entity.Certificate;
import com.epam.esm.service.dto.TagDto;
import com.epam.esm.service.mapper.Mapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashSet;

@RunWith(MockitoJUnitRunner.class)
public class CertificateMapperImplTest {
    @Mock
    private Mapper<Tag, TagDto> tagMapper;
    @InjectMocks
    private CertificateMapperImpl testedMapper;
    private static Certificate certificate;
    private static CertificateDto dto;
    private static Tag tag;
    private static TagDto tagDto;

    @Before
    public void initialize(){
        certificate = new Certificate();
        certificate.setId(10);
        certificate.setName("test");
        certificate.setDescription("description");
        certificate.setPrice(BigDecimal.valueOf(20.00));
        certificate.setDateOfCreation(LocalDate.of(2030, 12, 12));
        certificate.setDateOfModification(LocalDate.of(2030, 12,12));
        certificate.setDateOfExpiration(LocalDate.of(2030,12,12));
        tag = new Tag();
        tag.setId(1);
        tag.setName("test");
        HashSet<Tag> tags = new HashSet<>();
        tags.add(tag);
        certificate.setTags(tags);

        dto = new CertificateDto();
        dto.setId(10);
        dto.setName("test");
        dto.setDescription("description");
        dto.setDescription("description");
        dto.setPrice(BigDecimal.valueOf(20.00));
        dto.setDateOfCreation(Date.valueOf(LocalDate.of(2030,12,12)));
        dto.setDateOfModification((Date.valueOf(LocalDate.of(2030,12,12))));
        dto.setDateOfExpiration((Date.valueOf(LocalDate.of(2030,12,12))));
        tagDto = new TagDto();
        tagDto.setId(1);
        tagDto.setName("test");
        HashSet<TagDto> tagDtos = new HashSet<>();
        tagDtos.add(tagDto);
        dto.setTags(tagDtos);

        Mockito.when(tagMapper.toEntity(tagDto)).thenReturn(tag);
        Mockito.when(tagMapper.toDto(tag)).thenReturn(tagDto);
    }

    @Test
    public void testToEntityWithNull() {
        //given

        //when

        //then
        Assert.assertNull(testedMapper.toEntity(null));
    }

    @Test
    public void testToEntityWithNonNull() {
        //given

        //when
        Certificate actual = testedMapper.toEntity(dto);

        //then
        Mockito.verify(tagMapper).toEntity(tagDto);
        Assert.assertEquals(certificate, actual);
    }

    @Test
    public void testToDtoNull() {
        //given

        //when

        //then
        Assert.assertNull(testedMapper.toDto(null));
    }

    @Test
    public void testToDtoWithNonNull() {
        //given

        //when
        CertificateDto actual = testedMapper.toDto(certificate);

        //then
        Mockito.verify(tagMapper).toDto(tag);
        Assert.assertEquals(dto, actual);
    }

    @Test
    public void testToDtoWithNullsAsValue(){
        //given
        certificate.setDateOfCreation(null);
        certificate.setDateOfModification(null);
        certificate.setDateOfExpiration(null);
        dto.setDateOfCreation(null);
        dto.setDateOfModification(null);
        dto.setDateOfExpiration(null);

        //when
        CertificateDto actual = testedMapper.toDto(certificate);

        //then
        Mockito.verify(tagMapper).toDto(tag);
        Assert.assertEquals(dto, actual);
    }

    @Test
    public void testToEntityWithNullsAsValue(){
        //given
        certificate.setDateOfCreation(null);
        certificate.setDateOfModification(null);
        certificate.setDateOfExpiration(null);
        dto.setDateOfCreation(null);
        dto.setDateOfModification(null);
        dto.setDateOfExpiration(null);

        //when
        Certificate actual = testedMapper.toEntity(dto);

        //then
        Assert.assertEquals(certificate, actual);
    }
}
