package com.epam.esm.service.service.impl;

import com.epam.esm.core.entity.Certificate;
import com.epam.esm.core.entity.Tag;
import com.epam.esm.core.exception.RepositoryException;
import com.epam.esm.core.repository.EntityRepository;
import com.epam.esm.core.repository.TagRepository;
import com.epam.esm.core.specification.Specification;
import com.epam.esm.service.dto.CertificateDto;
import com.epam.esm.service.dto.TagDto;
import com.epam.esm.service.exception.ServiceException;
import com.epam.esm.service.mapper.Mapper;
import com.epam.esm.service.specification.SpecificationConverter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.MultiValueMap;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.*;

@RunWith(MockitoJUnitRunner.class)
public class CertificateServiceImplTest {
    @Mock
    private EntityRepository<Certificate> certificateRepository;
    @Mock
    private TagRepository tagRepository;
    @Mock
    private Mapper<Certificate, CertificateDto> mapper;
    @Mock
    private SpecificationConverter converter;
    @Mock
    private MultiValueMap<String, String> parameters;
    @Mock
    private Specification<Tag> specification;
    @InjectMocks
    private CertificateServiceImpl testedService =
            new CertificateServiceImpl(certificateRepository, tagRepository, mapper, converter);
    private Certificate certificate = new Certificate();
    private CertificateDto dto = new CertificateDto();
    private Tag tag;
    private Set<Tag> tags;
    private Set<TagDto> tagDtos;

    @Before
    public void initialize() {
        certificate.setId(1);
        certificate.setName("Name");
        certificate.setDescription("Description");
        certificate.setPrice(BigDecimal.ZERO);
        certificate.setDateOfCreation(LocalDate.of(2030, 12, 12));
        certificate.setDateOfModification(LocalDate.of(2030, 12, 12));
        certificate.setDateOfExpiration(LocalDate.of(2030, 12, 12));
        tags = new HashSet<>();
        tag = new Tag();
        tag.setId(1);
        tag.setName("test");
        tags.add(tag);
        certificate.setTags(tags);

        dto.setId(1);
        dto.setName("Name");
        dto.setDescription("Description");
        dto.setPrice(BigDecimal.ZERO);
        dto.setDateOfCreation(Date.valueOf(LocalDate.of(2030, 12, 12)));
        dto.setDateOfModification(Date.valueOf(LocalDate.of(2030, 12, 12)));
        dto.setDateOfExpiration(Date.valueOf(LocalDate.of(2030, 12, 12)));
        tagDtos = new HashSet<>();
        TagDto tagDto = new TagDto();
        tagDto.setId(1);
        tagDto.setName("test");
        tagDtos.add(tagDto);
        dto.setTags(tagDtos);
    }

    @Before
    public void resetMocks() {
        Mockito.reset(certificateRepository, converter, mapper);
        Mockito.when(mapper.toDto(certificate)).thenReturn(dto);
        Mockito.when(mapper.toEntity(dto)).thenReturn(certificate);
    }

    @Test
    public void testGetById() throws RepositoryException, ServiceException {
        //given
        Mockito.when(certificateRepository.getById(1)).thenReturn(certificate);

        //when
        CertificateDto actual = testedService.getById(1);

        //then
        Assert.assertEquals(actual, dto);
        Mockito.verify(certificateRepository).getById(1);
        Mockito.verify(mapper).toDto(certificate);
    }

    @Test(expected = ServiceException.class)
    public void testGetByIdThrowsException() throws RepositoryException, ServiceException {
        //given
        Mockito.when(certificateRepository.getById(1)).thenThrow(RepositoryException.class);

        //when
        testedService.getById(1);

        //then
        Mockito.verify(certificateRepository).getById(1);
        Mockito.verify(mapper).toDto(certificate);
        Assert.fail();
    }

    @Test
    public void testAdd() throws RepositoryException, ServiceException {
        //given
        Mockito.when(certificateRepository.add(certificate)).thenReturn(certificate);
        Mockito.when(tagRepository.addOrGetByName(tag)).thenReturn(tag);
        Mockito.when(certificateRepository.getById(Mockito.anyLong())).thenReturn(certificate);

        //when
        CertificateDto actual = testedService.add(dto);

        //then
        Mockito.verify(certificateRepository).add(certificate);
        Mockito.verify(tagRepository).addOrGetByName(tag);
        Mockito.verify(mapper).toEntity(dto);
        Assert.assertEquals(dto, actual);
    }

    @Test(expected = ServiceException.class)
    public void testAddThrowsException() throws RepositoryException, ServiceException {
        //given
        Mockito.when(certificateRepository.add(certificate)).thenThrow(RepositoryException.class);

        //when
        testedService.add(dto);

        //then
        Mockito.verify(certificateRepository).add(certificate);
        Mockito.verify(mapper).toDto(certificate);
        Mockito.verify(mapper).toEntity(dto);
        Assert.fail();
    }

    @Test
    public void testUpdate() throws RepositoryException, ServiceException {
        //given
        Mockito.when(certificateRepository.update(certificate)).thenReturn(certificate);
        Mockito.when(tagRepository.addOrGetByName(tag)).thenReturn(tag);
        Mockito.when(certificateRepository.getById(Mockito.anyLong())).thenReturn(certificate);

        //when
        CertificateDto actual = testedService.update(dto);

        //then
        Assert.assertEquals(dto, actual);
        Mockito.verify(certificateRepository).update(certificate);
        Mockito.verify(tagRepository).addOrGetByName(tag);
        Mockito.verify(mapper).toDto(certificate);
        Mockito.verify(mapper).toEntity(dto);
    }

    @Test(expected = ServiceException.class)
    public void testUpdateThrowsException() throws RepositoryException, ServiceException {
        //given
        Mockito.when(certificateRepository.update(certificate)).thenThrow(RepositoryException.class);

        //when
        testedService.update(dto);

        //then
        Mockito.verify(certificateRepository).update(certificate);
        Mockito.verify(mapper).toDto(certificate);
        Mockito.verify(mapper).toEntity(dto);
        Assert.fail();
    }

    @Test
    public void testPatch() throws RepositoryException, ServiceException {
        //given
        CertificateDto patchDto = new CertificateDto();
        patchDto.setId(certificate.getId());
        patchDto.setPrice(BigDecimal.TEN);
        Certificate patch = new Certificate();
        patch.setId(patchDto.getId());
        patch.setPrice(patchDto.getPrice());
        Certificate patchedCertificate = certificate;
        patchedCertificate.setPrice(patch.getPrice());
        Mockito.when(certificateRepository.getById(dto.getId())).thenReturn(certificate);
        Mockito.when(mapper.toEntity(patchDto)).thenReturn(patch);
        Mockito.when(mapper.patchEntityToEntitySkipNulls(certificate, patch)).thenReturn(patchedCertificate);
        Mockito.when(certificateRepository.update(patchedCertificate)).thenReturn(patchedCertificate);

        //when

        //then
    }

    @Test
    public void testRemove() throws ServiceException, RepositoryException {
        //given
        Mockito.when(certificateRepository.getById(dto.getId())).thenReturn(certificate);

        //when
        testedService.remove(dto);

        //then
        Mockito.verify(certificateRepository).remove(certificate);
    }

    @Test(expected = ServiceException.class)
    public void testRemoveThrowsException() throws ServiceException, RepositoryException {
        //given
        Mockito.when(certificateRepository.getById(dto.getId())).thenThrow(new RepositoryException());

        //when
        testedService.remove(dto);

        //then
        Mockito.verify(certificateRepository).getById(dto.getId());
    }

    @Test
    public void testQuery() throws ServiceException, RepositoryException {
        //given
        List<Certificate> certificates = new ArrayList<>();
        certificates.add(certificate);
        Mockito.when(certificateRepository.query(Mockito.any(Specification.class))).thenReturn(certificates);
        List<CertificateDto> expected = new ArrayList<>();
        expected.add(dto);

        //when
        List<CertificateDto> actual = testedService.query(parameters);

        //then
        Mockito.verify(converter).convert(Mockito.any(MultiValueMap.class), Mockito.any(Specification.class));
        Mockito.verify(certificateRepository).query(Mockito.any(Specification.class));
        Mockito.verify(mapper, Mockito.times(certificates.size())).toDto(certificate);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = ServiceException.class)
    public void testQueryThrowsException() throws ServiceException, RepositoryException {
        //given
        Mockito.when(certificateRepository.query(Mockito.any(Specification.class))).thenThrow(new RepositoryException());

        //when
        testedService.query(parameters);

        //then
        Mockito.verify(certificateRepository.query(Mockito.any(Specification.class)));
    }
}
