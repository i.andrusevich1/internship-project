package com.epam.esm.service.specification;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = ServiceSpringConfiguration.class)
//public class SpecificationConverterTest {
//    @Autowired
//    private SpecificationConverter testedConverter;
//    private static ServiceSpecification serviceSpecification = new ServiceSpecification();
//    private static RepositorySpecification expected = new CertificateRepositorySpecificationImpl();
//
//    @BeforeClass
//    public static void initialize() {
//        Map<String, String> filterParameters = new HashMap<>();
//        filterParameters.put("name", "searchedName");
//        filterParameters.put("namePart", "part");
//        Map<String, Boolean> sortParameters = new HashMap<>();
//        sortParameters.put("id", false);
//        sortParameters.put("name", true);
//        serviceSpecification.setFilterParameters(filterParameters);
//        serviceSpecification.setSortParameters(sortParameters);
//
//        expected.acceptFilterParameter("NAME", "searchedName");
//        expected.acceptFilterParameter("NAMEPART", "part");
//        expected.acceptSortParameter("ID", false);
//        expected.acceptSortParameter("NAME", true);
//    }

//    @Test
//    public void testConvert() {
//        //given
//        RepositorySpecification actual = new CertificateRepositorySpecificationImpl();
//
//        //when
//        testedConverter.convert(serviceSpecification, actual);
//
//        //then
//        Assert.assertEquals(expected, actual);
//    }
//}
