package com.epam.esm.service.service.impl;

import com.epam.esm.core.entity.Tag;
import com.epam.esm.core.exception.RepositoryException;
import com.epam.esm.core.repository.TagRepository;
import com.epam.esm.core.specification.Specification;
import com.epam.esm.service.dto.TagDto;
import com.epam.esm.service.exception.ServiceException;
import com.epam.esm.service.mapper.Mapper;
import com.epam.esm.service.specification.SpecificationConverter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceImplTest {
    @Mock
    private TagRepository repository;
    @Mock
    private Mapper<Tag, TagDto> mapper;
    @Mock
    private SpecificationConverter converter;
    @Mock
    private MultiValueMap<String, String> parameters;
    @InjectMocks
    private TagServiceImpl testedService;
    private static Tag tag = new Tag();
    private static TagDto dto = new TagDto();

    @BeforeClass
    public static void initialize() {
        tag.setId(1);
        tag.setName("Name");
        dto.setId(1);
        dto.setName("Name");
    }

    @Before
    public void resetMocks() {
        Mockito.reset(repository, converter, mapper);
        Mockito.when(mapper.toDto(tag)).thenReturn(dto);
        Mockito.when(mapper.toEntity(dto)).thenReturn(tag);
    }

    @Test
    public void testGetById() throws RepositoryException, ServiceException {
        //given
        Mockito.when(repository.getById(1)).thenReturn(tag);

        //when
        TagDto actual = testedService.getById(1);

        //then
        Assert.assertEquals(actual, dto);
        Mockito.verify(repository).getById(1);
        Mockito.verify(mapper).toDto(tag);
    }

    @Test(expected = ServiceException.class)
    public void testGetByIdThrowsException() throws RepositoryException, ServiceException {
        //given
        Mockito.when(repository.getById(1)).thenThrow(RepositoryException.class);

        //when
        testedService.getById(1);

        //then
        Mockito.verify(repository).getById(1);
        Mockito.verify(mapper).toDto(tag);
        Assert.fail();
    }

    @Test
    public void testAdd() throws RepositoryException, ServiceException {
        //given
        Mockito.when(repository.add(tag)).thenReturn(tag);

        //when
        TagDto actual = testedService.add(dto);

        //then
        Mockito.verify(repository).add(tag);
        Mockito.verify(mapper).toDto(tag);
        Mockito.verify(mapper).toEntity(dto);
        Assert.assertEquals(dto, actual);
    }

    @Test(expected = ServiceException.class)
    public void testAddThrowsException() throws RepositoryException, ServiceException {
        //given
        Mockito.when(repository.add(tag)).thenThrow(RepositoryException.class);

        //when
        testedService.add(dto);

        //then
        Mockito.verify(repository).add(tag);
        Mockito.verify(mapper).toDto(tag);
        Mockito.verify(mapper).toEntity(dto);
        Assert.fail();
    }

    @Test
    public void testUpdate() throws RepositoryException, ServiceException {
        //given
        Mockito.when(repository.update(tag)).thenReturn(tag);

        //when
        TagDto actual = testedService.update(dto);

        //then
        Mockito.verify(repository).update(tag);
        Mockito.verify(mapper).toDto(tag);
        Mockito.verify(mapper).toEntity(dto);
        Assert.assertEquals(dto, actual);
    }

    @Test(expected = ServiceException.class)
    public void testUpdateThrowsException() throws RepositoryException, ServiceException {
        //given
        Mockito.when(repository.update(tag)).thenThrow(RepositoryException.class);

        //when
        testedService.update(dto);

        //then
        Mockito.verify(repository.update(tag));
        Mockito.verify(mapper).toDto(tag);
        Mockito.verify(mapper).toEntity(dto);
        Assert.fail();
    }

    @Test
    public void testRemove() throws ServiceException, RepositoryException {
        //given
        Mockito.when(repository.getById(dto.getId())).thenReturn(tag);

        //when
        testedService.remove(dto);

        //then
        Mockito.verify(repository).remove(tag);
    }

    @Test
    public void testQuery() throws ServiceException, RepositoryException {
        //given
        List<Tag> tags = new ArrayList<>();
        tags.add(tag);
        Mockito.when(repository.query(Mockito.any(Specification.class))).thenReturn(tags);
        List<TagDto> expected = new ArrayList<>();
        expected.add(dto);

        //when
        List<TagDto> actual = testedService.query(parameters);

        //then
        Mockito.verify(converter).convert(Mockito.any(MultiValueMap.class), Mockito.any(Specification.class));
        Mockito.verify(repository).query(Mockito.any(Specification.class));
        Mockito.verify(mapper, Mockito.times(tags.size())).toDto(tag);
        Assert.assertEquals(expected, actual);
    }
}
