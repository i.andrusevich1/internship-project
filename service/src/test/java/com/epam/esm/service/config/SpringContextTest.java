package com.epam.esm.service.config;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ServiceSpringConfiguration.class)
public class SpringContextTest {
    @Autowired
    private ApplicationContext context;

    @Test
    public void testIfContextCreated(){
        //given

        //when

        //then
        Assert.assertNotNull(context);
    }
}
