package com.epam.esm.service.mapper.impl;

import com.epam.esm.service.dto.TagDto;
import com.epam.esm.core.entity.Tag;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;

public class TagMapperImplTest {
    private ModelMapper mapper = Mockito.mock(ModelMapper.class);
    private Tag tag = Mockito.mock(Tag.class);
    private TagDto tagDto = Mockito.mock(TagDto.class);
    private TagMapperImpl testedMapper = new TagMapperImpl(mapper);

    @Test
    public void testToEntityWithNull(){
        //given

        //when

        //then
        Assert.assertNull(testedMapper.toEntity(null));
    }

    @Test
    public void testToEntityWithNonNull(){
        //given
        Mockito.when(mapper.map(tagDto, Tag.class)).thenReturn(tag);

        //when
        Tag actual = testedMapper.toEntity(tagDto);

        //then
        Assert.assertEquals(tag, actual);
        Mockito.verify(mapper).map(tagDto, Tag.class);
    }

    @Test
    public void testToDtoNull(){
        //given

        //when

        //then
        Assert.assertNull(testedMapper.toDto(null));
    }

    @Test
    public void testToDtoWithNonNull(){
        //given
        Mockito.when(mapper.map(tag, TagDto.class)).thenReturn(tagDto);

        //when
        TagDto actual = testedMapper.toDto(tag);

        //then
        Mockito.verify(mapper).map(tag, TagDto.class);
        Assert.assertEquals(tagDto, actual);
    }
}
