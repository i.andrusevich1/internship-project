package com.epam.esm.service.dto;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Objects;
import java.util.Set;

public class CertificateDto {
    private long id;
    private String name;
    private String description;
    private BigDecimal price;
    private Date dateOfCreation;
    private Date dateOfModification;
    private Date dateOfExpiration;
    private Set<TagDto> tags;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public Date getDateOfModification() {
        return dateOfModification;
    }

    public void setDateOfModification(Date dateOfModification) {
        this.dateOfModification = dateOfModification;
    }

    public Date getDateOfExpiration() {
        return dateOfExpiration;
    }

    public void setDateOfExpiration(Date dateOfExpiration) {
        this.dateOfExpiration = dateOfExpiration;
    }

    public Set<TagDto> getTags() {
        return tags;
    }

    public void setTags(Set<TagDto> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CertificateDto dto = (CertificateDto) o;
        return id == dto.id &&
                Objects.equals(name, dto.name) &&
                Objects.equals(description, dto.description) &&
                Objects.equals(price, dto.price) &&
                Objects.equals(dateOfCreation, dto.dateOfCreation) &&
                Objects.equals(dateOfModification, dto.dateOfModification) &&
                Objects.equals(dateOfExpiration, dto.dateOfExpiration) &&
                Objects.equals(tags, dto.tags);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, price, dateOfCreation, dateOfModification, dateOfExpiration, tags);
    }
}
