package com.epam.esm.service.mapper.impl;

import com.epam.esm.core.entity.Certificate;
import com.epam.esm.core.entity.Purchase;
import com.epam.esm.service.dto.CertificateDto;
import com.epam.esm.service.dto.PurchaseDto;
import com.epam.esm.service.mapper.Mapper;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class PurchaseMapperImpl implements Mapper<Purchase, PurchaseDto> {
    private ModelMapper mapper;
    private Mapper<Certificate, CertificateDto> certificateMapper;

    public PurchaseMapperImpl(ModelMapper mapper, Mapper<Certificate, CertificateDto> certificateMapper) {
        this.mapper = mapper;
        this.certificateMapper = certificateMapper;
    }

    @Override
    public Purchase toEntity(PurchaseDto dto) {
        if (dto == null){
            return null;
        }
        Purchase purchase = new Purchase();
        purchase.setId(dto.getId());
        List<CertificateDto> certificateDtos = dto.getCertificates();
        purchase.setCertificates((certificateDtos == null)
                ? new ArrayList<>()
                : certificateDtos
                .stream()
                .map(x -> certificateMapper.toEntity(x))
                .collect(Collectors.toList()));
        return purchase;
    }

    @Override
    public Purchase patchEntityToEntitySkipNulls(Purchase patch, Purchase dtoToApplyPatch) {
        mapper.map(patch, dtoToApplyPatch);
        return dtoToApplyPatch;
    }

    @Override
    public PurchaseDto toDto(Purchase purchase) {
        if (purchase == null){
            return null;
        }
        PurchaseDto dto = new PurchaseDto();
        dto.setId(purchase.getId());
        dto.setCreated(Date.from(purchase.getCreated().atZone(ZoneId.systemDefault()).toInstant()));
        dto.setCertificates(purchase
                .getCertificates()
                .stream()
                .map(x -> certificateMapper.toDto(x))
                .collect(Collectors.toList()));
        BigDecimal totalCost = BigDecimal.ZERO;
        for (CertificateDto certificate : dto.getCertificates()){
            totalCost = totalCost.add(certificate.getPrice());
        }
        dto.setTotalCost(totalCost);
        return dto;
    }
}
