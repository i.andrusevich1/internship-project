package com.epam.esm.service.mapper.impl;

import com.epam.esm.service.dto.TagDto;
import com.epam.esm.service.mapper.Mapper;
import com.epam.esm.core.entity.Tag;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class TagMapperImpl implements Mapper<Tag, TagDto> {
    private ModelMapper mapper;

    @Autowired
    public TagMapperImpl(ModelMapper mapper){
        this.mapper = mapper;
    }

    @Override
    public Tag toEntity(TagDto dto) {
        return Objects.isNull(dto) ? null : mapper.map(dto, Tag.class);
    }

    @Override
    public Tag patchEntityToEntitySkipNulls(Tag patch, Tag dtoToApplyPatch) {
        mapper.map(patch, dtoToApplyPatch);
        return dtoToApplyPatch;
    }

    @Override
    public TagDto toDto(Tag tag) {
        return Objects.isNull(tag) ? null : mapper.map(tag, TagDto.class);
    }
}
