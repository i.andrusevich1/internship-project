package com.epam.esm.service.service.impl;

import com.epam.esm.core.exception.RepositoryException;
import com.epam.esm.core.repository.TagRepository;
import com.epam.esm.core.specification.Specification;
import com.epam.esm.core.specification.impl.parameter.TagParameters;
import com.epam.esm.service.exception.EntityAlreadyExistsException;
import com.epam.esm.service.exception.ServiceException;
import com.epam.esm.service.mapper.Mapper;
import com.epam.esm.core.entity.Tag;
import com.epam.esm.service.dto.TagDto;
import com.epam.esm.service.service.TagService;
import com.epam.esm.service.specification.SpecificationConverter;
import com.epam.esm.core.specification.impl.TagSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.List;

@Component
@Transactional // needed even in get-methods to make possible retrieval of inner lazily-loaded entities
               // (they can be loaded only within the same session which gets closed without @Transactional)
public class TagServiceImpl implements TagService {
    private TagRepository repository;
    private Mapper<Tag, TagDto> mapper;
    private SpecificationConverter converter;

    @Autowired
    public TagServiceImpl(TagRepository repository, Mapper<Tag, TagDto> mapper, SpecificationConverter converter) {
        this.repository = repository;
        this.mapper = mapper;
        this.converter = converter;
    }

    @Override
    public TagDto getById(long id) throws ServiceException {
        try {
            return mapper.toDto(repository.getById(id));
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public TagDto add(TagDto tagDto) throws ServiceException {
        try {
            List<Tag> tags = repository.query(new TagSpecification(TagParameters.NAME, tagDto.getName()));
            if (!tags.isEmpty()) {
                throw new ServiceException(
                        new EntityAlreadyExistsException("Tag with name " + tagDto.getName() + " already exists"));
            }
            Tag tag = mapper.toEntity(tagDto);
            return mapper.toDto(repository.add(tag));
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public TagDto update(TagDto tagDto) throws ServiceException {
        Tag tag = mapper.toEntity(tagDto);
        try {
            return mapper.toDto(repository.update(tag));
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public TagDto patch(TagDto tagDto) throws ServiceException {
        try {
            Tag existingEntity = repository.getById(tagDto.getId());
            Tag patch = mapper.toEntity(tagDto);
            Tag patchedEntity = mapper.patchEntityToEntitySkipNulls(patch, existingEntity);
            repository.update(patchedEntity);
            return getById(patchedEntity.getId());
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void remove(TagDto tagDto) throws ServiceException {
        try {
            repository.remove(repository.getById(tagDto.getId()));
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<TagDto> query(MultiValueMap<String, String> parameters) throws ServiceException {
        Specification<Tag> specification = new TagSpecification();
        converter.convert(parameters, specification);
        List<TagDto> dtos = new ArrayList<>();
        try {
            repository.query(specification)
                    .forEach(x -> dtos.add(mapper.toDto(x)));
            return dtos;
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<TagDto> getMostMoneySpentUserTagsByPopularity() throws ServiceException {
        List<TagDto> dtos = new ArrayList<>();
        try {
            repository.getMostMonetSpentUserTagsByPopularity()
                    .forEach(x -> dtos.add(mapper.toDto(x)));
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
        return dtos;
    }
}
