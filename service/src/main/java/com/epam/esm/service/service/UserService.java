package com.epam.esm.service.service;

import com.epam.esm.service.dto.UserDto;
import com.epam.esm.service.exception.ServiceException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface UserService extends UserDetailsService {
    /**
     * Retrieves entity from a storage by ID
     *
     * @param id - Entity Id to be retrieved from a storage
     * @return - representation of the Entity retrieved from storage
     * @throws ServiceException in case something goes wrong while retrieving the entity
     */
    UserDto getById(long id) throws ServiceException;

    /**
     * Saves entity into a storage.
     *
     * @param item - representation of the Entity to be added to a storage
     * @return - representation of the just-added Entity retrieved from a storage
     * @throws ServiceException in case something goes wrong while retrieving back the added entity
     */
    UserDto add(UserDto item) throws ServiceException;

    /**
     * Replaces all the entity parameters.
     * All the entity fields get rewritten (accept Id) even in case they are null
     *
     * @param item - representation of the Entity to be updated in a storage.
     *             All the entity field will be rewritten accept Id
     * @return - representation of the just-updated Entity retrieved from a storage
     * @throws ServiceException in case something goes wrong while retrieving back the updated entity
     */
    UserDto update(UserDto item) throws ServiceException;

    UserDto patch(UserDto item) throws ServiceException;

    /**
     * Deletes entity from a storage
     *
     * @param item - representation of the Entity to be deleted from a storage.
     * @throws ServiceException in case something goes wrong while deleting the entity
     */
    void remove(UserDto item) throws ServiceException;


    /**
     * Retrieves from a storage all the entities that satisfy the provided specification
     *
     * @param parameters - map of filter and sort parameters where either key=filter_name and value=list_of_filter_values
     *                   or key=sortBy nad key=list_of_sort_parameters.
     *                   Order is DESC in case sort parameter starts with '-' and ASC if not
     * @return - list of representations of entities retrieved from a storage which satisfy the specification
     */
    List<UserDto> query(MultiValueMap<String, String> parameters) throws ServiceException;
}
