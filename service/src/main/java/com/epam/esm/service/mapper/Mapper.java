package com.epam.esm.service.mapper;

public interface Mapper<T, V> {
    /**
     * Converts the DTO-object into correspondent Entity
     *
     * @param o - DTO-object to ve converted to Entity-object
     * @return - Entity object
     */
    T toEntity(V o);

    T patchEntityToEntitySkipNulls(T patch, T dtoToApplyPatch);

    /**
     * Converts the Entity into corresponding DTO-object
     *
     * @param o - Entity object to be converted to DTO-object
     * @return - DTO-object
     */
    V toDto(T o);
}
