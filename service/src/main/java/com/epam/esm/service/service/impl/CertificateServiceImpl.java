package com.epam.esm.service.service.impl;

import com.epam.esm.core.entity.Tag;
import com.epam.esm.core.exception.RepositoryException;
import com.epam.esm.core.repository.EntityRepository;
import com.epam.esm.core.repository.TagRepository;
import com.epam.esm.core.specification.Specification;
import com.epam.esm.service.exception.ServiceException;
import com.epam.esm.service.mapper.Mapper;
import com.epam.esm.core.entity.Certificate;
import com.epam.esm.service.dto.CertificateDto;
import com.epam.esm.service.service.CertificateService;
import com.epam.esm.service.specification.SpecificationConverter;
import com.epam.esm.core.specification.impl.CertificateSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Transactional // needed even in get-methods to make possible retrieval of inner lazily-loaded entities
               // (they can be loaded only within the same session which gets closed without @Transactional)
public class CertificateServiceImpl implements CertificateService {
    private EntityRepository<Certificate>  certificateRepository;
    private TagRepository tagRepository;
    private Mapper<Certificate, CertificateDto> mapper;
    private SpecificationConverter converter;

    @Autowired
    public CertificateServiceImpl(EntityRepository<Certificate> certificateRepository, TagRepository tagRepository,
                                  Mapper<Certificate, CertificateDto> mapper, SpecificationConverter converter) {
        this.certificateRepository = certificateRepository;
        this.tagRepository = tagRepository;
        this.mapper = mapper;
        this.converter = converter;
    }

    @Override
    public CertificateDto getById(long id) throws ServiceException {
        try {
            Certificate certificate = certificateRepository.getById(id);
            return mapper.toDto(certificate);
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public CertificateDto add(CertificateDto certificateDto) throws ServiceException {
        Certificate certificate = mapper.toEntity(certificateDto);
        try {
            certificate.setTags(fetchIdsToTags(certificate.getTags()));
            certificateRepository.add(certificate);
            return getById(certificate.getId());
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public CertificateDto update(CertificateDto certificateDto) throws ServiceException {
        Certificate certificate = mapper.toEntity(certificateDto);
        try {
            certificate.setTags(fetchIdsToTags(certificate.getTags()));
            certificateRepository.update(certificate);
            return getById(certificate.getId());
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public CertificateDto patch(CertificateDto certificateDto) throws ServiceException {
        try {
            Certificate existingEntity = certificateRepository.getById(certificateDto.getId());
            Certificate patch = mapper.toEntity(certificateDto);
            Certificate patchedEntity = mapper.patchEntityToEntitySkipNulls(patch, existingEntity);
            if (patch.getTags() != null && patch.getTags().isEmpty()) {
                patchedEntity.setTags(patch.getTags());
            } else if (patch.getTags() != null) {
                patchedEntity.setTags(fetchIdsToTags(patch.getTags()));
            }
            certificateRepository.update(patchedEntity);
            return getById(patchedEntity.getId());
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }

    }


    @Override
    public void remove(CertificateDto certificateDto) throws ServiceException {
        try {
            certificateRepository.remove(certificateRepository.getById(certificateDto.getId()));
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<CertificateDto> query(MultiValueMap<String, String> params) throws ServiceException {
        try {
            Specification<Certificate> specification = new CertificateSpecification();
            converter.convert(params, specification);
            return certificateRepository
                    .query(specification)
                    .stream()
                    .map(x -> mapper.toDto(x))
                    .collect(Collectors.toList());
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    private Set<Tag> fetchIdsToTags(Set<Tag> tags) throws RepositoryException {
        Set<Tag> tagsWithIds = new HashSet<>();
        if (tags != null) {
            for (Tag tag : tags) {
                tagsWithIds.add(tagRepository.addOrGetByName(tag));
            }
        }
        return (tagsWithIds);
    }
}