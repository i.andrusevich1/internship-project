package com.epam.esm.service.specification;

import com.epam.esm.core.exception.RepositoryException;
import com.epam.esm.core.specification.Specification;
import com.epam.esm.service.exception.ServiceException;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.Map;

@Component
public class SpecificationConverter {
    private static final String SORT_PARAMETER_NAME = "sortBy";
    private static final String ITEMS_PER_PAGE_PARAMETER_NAME = "itemsPerPage";
    private static final String CURRENT_PAGE_PARAMETER_NAME = "currentPage";


    public void convert(MultiValueMap<String, String> parameters, Specification specification) throws ServiceException {
        try {
            convertSortParameters(parameters, specification);
            convertPagination(parameters, specification);
            convertFilterParameters(parameters, specification);
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    private void convertSortParameters(MultiValueMap<String, String> parameters, Specification specification)
            throws RepositoryException {
        List<String> sortParameters = parameters.remove(SORT_PARAMETER_NAME);
        if (sortParameters == null) {
            return;
        }
        for (String sorting : sortParameters) {
            if (sorting.startsWith("-")) {
                specification.acceptSortParameter(sorting.substring(1), false);
            } else {
                specification.acceptSortParameter(sorting, true);
            }
        }
    }

    private void convertPagination(MultiValueMap<String, String> parameters, Specification specification) {
        List<String> itemsPerPageValues = parameters.remove(ITEMS_PER_PAGE_PARAMETER_NAME);
        if (itemsPerPageValues != null){
            specification.setItemsPerPage(Integer.parseInt(itemsPerPageValues.get(0)));
        }

        List<String> currentPageValues = parameters.remove(CURRENT_PAGE_PARAMETER_NAME);
        if (currentPageValues != null) {
            specification.setCurrentPage(Integer.parseInt(currentPageValues.get(0)));
        }
    }

    private void convertFilterParameters(MultiValueMap<String, String> parameters, Specification specification)
            throws RepositoryException {
        for (Map.Entry<String, List<String>> entry : parameters.entrySet()) {
            String filter = entry.getKey();
            List<String> values = entry.getValue();
            specification.acceptFilterParameter(filter, values);
        }
    }
}
