package com.epam.esm.service.service.impl;

import com.epam.esm.core.entity.Purchase;
import com.epam.esm.core.entity.User;
import com.epam.esm.core.exception.RepositoryException;
import com.epam.esm.core.repository.EntityRepository;
import com.epam.esm.core.specification.Specification;
import com.epam.esm.core.specification.impl.PurchaseSpecification;
import com.epam.esm.service.dto.PurchaseDto;
import com.epam.esm.service.exception.ServiceException;
import com.epam.esm.service.mapper.Mapper;
import com.epam.esm.service.service.PurchaseService;
import com.epam.esm.service.specification.SpecificationConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Transactional // needed even in get-methods to make possible retrieval of inner lazily-loaded entities
               // (they can be loaded only within the same session which gets closed without @Transactional)
public class PurchaseServiceImpl implements PurchaseService {
    private EntityRepository<Purchase> repository;
    private Mapper<Purchase, PurchaseDto> mapper;
    private SpecificationConverter converter;

    @Autowired
    public PurchaseServiceImpl(EntityRepository<Purchase> repository, UserDetailsService userService,
                               Mapper<Purchase, PurchaseDto> mapper, SpecificationConverter converter) {
        this.repository = repository;
        this.mapper = mapper;
        this.converter = converter;
    }

    @Override
    public PurchaseDto getById(long id) throws ServiceException {
        try {
            return mapper.toDto(repository.getById(id));
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public PurchaseDto add(PurchaseDto dto) throws ServiceException {
        Purchase purchase = mapper.toEntity(dto);
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        purchase.setUser(currentUser);
        try {
            repository.add(purchase);
            return getById(purchase.getId());
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public PurchaseDto update(PurchaseDto dto) throws ServiceException {
        return null;
    }

    @Override
    public PurchaseDto patch(PurchaseDto dto) throws ServiceException {
        return null;
    }

    @Override
    public void remove(PurchaseDto dto) throws ServiceException {

    }

    @Override
    public List<PurchaseDto> query(MultiValueMap<String, String> parameters) throws ServiceException {
        try {
            Specification<Purchase> specification = new PurchaseSpecification();
            converter.convert(parameters, specification);
            return repository
                    .query(specification)
                    .stream()
                    .map(x -> mapper.toDto(x))
                    .collect(Collectors.toList());
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }
}
