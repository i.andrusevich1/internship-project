package com.epam.esm.service.mapper.impl;

import com.epam.esm.core.entity.User;
import com.epam.esm.service.dto.UserDto;
import com.epam.esm.service.mapper.Mapper;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class UserMapperImpl implements Mapper<User, UserDto> {
    private ModelMapper mapper;

    public UserMapperImpl(ModelMapper mapper){
        this.mapper = mapper;
    }

    @Override
    public User toEntity(UserDto dto) {
        return Objects.isNull(dto) ? null : mapper.map(dto, User.class);
    }

    @Override
    public User patchEntityToEntitySkipNulls(User patch, User dtoToApplyPatch) {
        mapper.map(patch, dtoToApplyPatch);
        return dtoToApplyPatch;
    }

    @Override
    public UserDto toDto(User user) {
        return Objects.isNull(user) ? null : mapper.map(user, UserDto.class);
    }
}
