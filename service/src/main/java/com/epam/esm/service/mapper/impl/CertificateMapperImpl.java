package com.epam.esm.service.mapper.impl;

import com.epam.esm.core.entity.Tag;
import com.epam.esm.service.dto.CertificateDto;
import com.epam.esm.service.dto.TagDto;
import com.epam.esm.service.mapper.Mapper;
import com.epam.esm.core.entity.Certificate;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class CertificateMapperImpl implements Mapper<Certificate, CertificateDto> {
    private Mapper<Tag, TagDto> tagMapper;
    private ModelMapper modelMapper;

    @Autowired
    public CertificateMapperImpl(Mapper<Tag, TagDto> tagMapper, ModelMapper modelMapper) {
        this.tagMapper = tagMapper;
        this.modelMapper = modelMapper;
    }

    @Override
    public Certificate toEntity(CertificateDto dto) {
        if (dto == null) {
            return null;
        }
        Certificate certificate = new Certificate();
        certificate.setId(dto.getId());
        certificate.setName(dto.getName());
        certificate.setDescription(dto.getDescription());
        certificate.setPrice(dto.getPrice());
        certificate.setDateOfCreation((dto.getDateOfCreation() == null)
                ? null : dto.getDateOfCreation().toLocalDate());
        certificate.setDateOfModification((dto.getDateOfModification() == null)
                ? null : dto.getDateOfModification().toLocalDate());
        certificate.setDateOfExpiration((dto.getDateOfExpiration() == null)
                ? null : dto.getDateOfExpiration().toLocalDate());
        Set<TagDto> tagDtos = dto.getTags();
        certificate.setTags((tagDtos == null) ? null : tagDtos.stream().map(x -> tagMapper.toEntity(x)).collect(Collectors.toSet()));
        return certificate;
    }

    @Override
    public Certificate patchEntityToEntitySkipNulls(Certificate patch, Certificate dtoToApplyPatch){
        modelMapper.map(patch, dtoToApplyPatch);
        return dtoToApplyPatch;
    }

    public CertificateDto toDto(Certificate certificate) {
        if (certificate == null) {
            return null;
        }
        CertificateDto dto = new CertificateDto();
        dto.setId(certificate.getId());
        dto.setName(certificate.getName());
        dto.setDescription(certificate.getDescription());
        dto.setPrice(certificate.getPrice());
        dto.setDateOfCreation((certificate.getDateOfCreation() == null)
                ? null : Date.valueOf(certificate.getDateOfCreation()));
        dto.setDateOfModification((certificate.getDateOfModification() == null)
                ? null : Date.valueOf(certificate.getDateOfModification()));
        dto.setDateOfExpiration((certificate.getDateOfExpiration() == null)
                ? null : Date.valueOf(certificate.getDateOfExpiration()));
        dto.setTags(certificate.getTags().stream().map(x -> tagMapper.toDto(x)).collect(Collectors.toSet()));
        return dto;
    }
}
