package com.epam.esm.service.service.impl;

import com.epam.esm.core.entity.User;
import com.epam.esm.core.exception.RepositoryException;
import com.epam.esm.core.repository.EntityRepository;
import com.epam.esm.core.specification.Specification;
import com.epam.esm.core.specification.impl.parameter.UserParameters;
import com.epam.esm.core.specification.impl.UserSpecification;
import com.epam.esm.service.dto.UserDto;
import com.epam.esm.service.exception.ServiceException;
import com.epam.esm.service.mapper.Mapper;
import com.epam.esm.service.service.UserService;
import com.epam.esm.service.specification.SpecificationConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Transactional // needed even in get-methods to make possible retrieval of inner lazily-loaded entities
               // (they can be loaded only within the same session which gets closed without @Transactional)
public class UserServiceImpl implements UserService {
    private static final String DEFAULT_ROLE = "User";
    private EntityRepository<User> repository;
    private SpecificationConverter converter;
    private Mapper<User, UserDto> mapper;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(EntityRepository<User> repository, SpecificationConverter converter,
                           Mapper<User, UserDto> mapper, PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.converter = converter;
        this.mapper = mapper;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDto getById(long id) throws ServiceException {
        try {
            return mapper.toDto(repository.getById(id));
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public UserDto add(UserDto dto) throws ServiceException {
        try {
            User user = mapper.toEntity(dto);
            user.setPassword(passwordEncoder.encode(dto.getPassword()));
            user.setRole(DEFAULT_ROLE);
            user.setEnabled(true);
            return mapper.toDto(repository.add(user));
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public UserDto update(UserDto dto) throws ServiceException {
        try {
            return mapper.toDto(repository.update(mapper.toEntity(dto)));
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public UserDto patch(UserDto dto) throws ServiceException {
        throw new ServiceException(new UnsupportedOperationException("Patch is not supported for in UserServiceImpl"));
    }

    @Override
    public void remove(UserDto dto) throws ServiceException {
        try {
            repository.remove(repository.getById(dto.getId()));
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<UserDto> query(MultiValueMap<String, String> parameters) throws ServiceException {
        Specification<User> specification = new UserSpecification();
        converter.convert(parameters, specification);
        try {
            return repository
                    .query(specification)
                    .stream()
                    .map(x -> mapper.toDto(x))
                    .collect(Collectors.toList());
        } catch (RepositoryException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        try {
            List<User> users = repository.query(new UserSpecification(UserParameters.USERNAME, username));
            if (users.isEmpty()) {
                throw new UsernameNotFoundException("User with username " + username + " was not found");
            }
            return users.get(0);
        } catch (RepositoryException e) {
            throw new UsernameNotFoundException(e.getMessage());
        }
    }
}
